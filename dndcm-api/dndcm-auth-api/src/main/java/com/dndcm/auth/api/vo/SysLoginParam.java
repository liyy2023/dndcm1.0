package com.dndcm.auth.api.vo;


import com.dndcm.commn.request.AbstractRequest;
import com.dndcm.commn.utils.AbstractParamUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lyy
 * @description
 * @date 2021-7-14 10:02:56
 **/
@Data
public class SysLoginParam extends AbstractRequest {

    @ApiModelProperty(value = "uuid", required = false)
    private String uuid;
    @ApiModelProperty(value = "用户名", required = false)
    private String username;
    @ApiModelProperty(value = "密码", required = false)
    private String password;
    @ApiModelProperty(value = "验证码", required = false)
    private String captcha;

    @Override
    public boolean isWrite() {
        return false;
    }

    @Override
    public void checkInput() {
        AbstractParamUtil.isBlank(captcha,"验证码不能为空");
    }
}
