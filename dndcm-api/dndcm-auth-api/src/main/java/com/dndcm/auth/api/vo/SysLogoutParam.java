package com.dndcm.auth.api.vo;

import com.dndcm.commn.request.AbstractRequest;
import com.dndcm.commn.utils.AbstractParamUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登出参数
 * @author lyy
 * @date 2021-7-14 10:29:50
 */
@Data
public class SysLogoutParam extends AbstractRequest {

    @ApiModelProperty(value = "id", required = true)
    private Long id;

    @Override
    public boolean isWrite() {
        return false;
    }

    @Override
    public void checkInput() {
        AbstractParamUtil.isBlank(id+"","用户id不能为空");
    }
}
