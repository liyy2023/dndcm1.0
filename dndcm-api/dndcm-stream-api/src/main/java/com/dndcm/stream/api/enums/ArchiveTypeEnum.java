package com.dndcm.stream.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 归档类型枚举
 * @author lyy
 * @date 2021-7-13 11:47:36
 */
@Getter
@AllArgsConstructor
public enum ArchiveTypeEnum {

    SCPZ(1,"上传凭证"),
    YWXT(2,"业务系统"),
    LZTDK(3,"来自图档库"),
    LZGH(4,"来自规划");

    private Integer code;
    private String desc;
}
