package com.dndcm.stream.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 项目流程部署表
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
@TableName("project_process_deployment")
@ApiModel(value="ProjectProcessDeployment对象", description="项目流程部署表")
public class ProjectProcessDeployment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "项目id")
    private Long projectId;

    @ApiModelProperty(value = "流程id")
    private Long processId;

    @ApiModelProperty(value = "节点id")
    private Long nodeId;

    @ApiModelProperty(value = "创建人id")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "是否删除(1-是，0-否)")
    private Integer deleteYn;

    @ApiModelProperty(value = "修改人id")
    private Long updataId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updataTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Integer getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(Integer deleteYn) {
        this.deleteYn = deleteYn;
    }

    public Long getUpdataId() {
        return updataId;
    }

    public void setUpdataId(Long updataId) {
        this.updataId = updataId;
    }

    public LocalDateTime getUpdataTime() {
        return updataTime;
    }

    public void setUpdataTime(LocalDateTime updataTime) {
        this.updataTime = updataTime;
    }

    @Override
    public String toString() {
        return "ProjectProcessDeployment{" +
        "id=" + id +
        ", projectId=" + projectId +
        ", processId=" + processId +
        ", nodeId=" + nodeId +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", deleteYn=" + deleteYn +
        ", updataId=" + updataId +
        ", updataTime=" + updataTime +
        "}";
    }
}
