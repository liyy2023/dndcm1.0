package com.dndcm.stream.api.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dndcm.stream.api.entity.ProjectCategory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 流程管理业务层接口
 * @author lyy
 * @date 2021-7-6 17:32:58
 */
@FeignClient(name = "dndcm-stream")
public interface TestFeign {

    /**
     * test
     * @param record
     * @return
     */
    @RequestMapping("/test/queryCategoryInfo")
    ProjectCategory queryApiList(@RequestBody ProjectCategory record);
}
