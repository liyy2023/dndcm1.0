package com.dndcm.stream.api.entity.vo;

import com.dndcm.stream.api.entity.TaskNodeMainConfig;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 流程详情vo
 * @author lyy
 * @date 2021-7-9 17:02:51
 */
@Data
public class ProcedureVo {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "流程名称")
    private String procedureName;

    @ApiModelProperty(value = "流程类别")
    private Integer procedureType;

    @ApiModelProperty(value = "流程描述")
    private String description;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "节点分页信息")
    private List<TaskNodeMainConfig> nodeList;

}
