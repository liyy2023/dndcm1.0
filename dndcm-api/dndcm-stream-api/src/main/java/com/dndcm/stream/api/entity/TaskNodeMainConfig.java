package com.dndcm.stream.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 任务节点配置表
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-13
 */
@TableName("task_node_main_config")
@ApiModel(value="TaskNodeMainConfig对象", description="任务节点配置表 ")
public class TaskNodeMainConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "流程定义id")
    private Long deployId;

    @ApiModelProperty(value = "任务id")
    private Long taskId;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "进度时限类型（1-周期；2-里程碑）")
    private Integer progressTimeType;

    @ApiModelProperty(value = "节点期限")
    private Integer nodeDeadline;

    @ApiModelProperty(value = "时限只取工作日(1-是；0-否)")
    private Integer deadlineWorkDayYn;

    @ApiModelProperty(value = "节点说明")
    private String nodeInstruction;

    @ApiModelProperty(value = "是否考核（1-是；0-否）")
    private Integer assessYn;

    @ApiModelProperty(value = "是否子工程（1-是；0-否）")
    private Integer subprojectYn;

    @ApiModelProperty(value = "电网图形融合点(1-无；2-方案编制；3-方案查看)")
    private String gridGraphicsFusionPoint;

    @ApiModelProperty(value = "前置节点（可能会有多个，用英文逗号间隔）")
    private String preTaskId;

    @ApiModelProperty(value = "完整性检查")
    private String integrityCheckType;

    @ApiModelProperty(value = "查看文档绑定")
    private String viewDocumentType;

    @ApiModelProperty(value = "绑定工程表单")
    private String bindingProjectFormType;

    @ApiModelProperty(value = "关联表单")
    private String relationFormType;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除（1-是，2否）")
    private Integer deleteYn;

    @ApiModelProperty(value = "节点序号")
    private Integer nodeNumber;

    @ApiModelProperty(value = "入库控制（ 1-规划库  2-储备库 3-计划库 4-执行库）")
    private Integer inStoreCtrl;

    @ApiModelProperty(value = "节点类别(1-开始，2-过程管控，3-结束，4-显示)")
    private Integer nodeType;

    @ApiModelProperty(value = "职责岗位")
    private Long positionJobsId;

    @ApiModelProperty(value = "绑定的子流程")
    private Long subprojectBindingId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeployId() {
        return deployId;
    }

    public void setDeployId(Long deployId) {
        this.deployId = deployId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getProgressTimeType() {
        return progressTimeType;
    }

    public void setProgressTimeType(Integer progressTimeType) {
        this.progressTimeType = progressTimeType;
    }

    public Integer getNodeDeadline() {
        return nodeDeadline;
    }

    public void setNodeDeadline(Integer nodeDeadline) {
        this.nodeDeadline = nodeDeadline;
    }

    public Integer getDeadlineWorkDayYn() {
        return deadlineWorkDayYn;
    }

    public void setDeadlineWorkDayYn(Integer deadlineWorkDayYn) {
        this.deadlineWorkDayYn = deadlineWorkDayYn;
    }

    public String getNodeInstruction() {
        return nodeInstruction;
    }

    public void setNodeInstruction(String nodeInstruction) {
        this.nodeInstruction = nodeInstruction;
    }

    public Integer getAssessYn() {
        return assessYn;
    }

    public void setAssessYn(Integer assessYn) {
        this.assessYn = assessYn;
    }

    public Integer getSubprojectYn() {
        return subprojectYn;
    }

    public void setSubprojectYn(Integer subprojectYn) {
        this.subprojectYn = subprojectYn;
    }

    public String getGridGraphicsFusionPoint() {
        return gridGraphicsFusionPoint;
    }

    public void setGridGraphicsFusionPoint(String gridGraphicsFusionPoint) {
        this.gridGraphicsFusionPoint = gridGraphicsFusionPoint;
    }

    public String getPreTaskId() {
        return preTaskId;
    }

    public void setPreTaskId(String preTaskId) {
        this.preTaskId = preTaskId;
    }

    public String getIntegrityCheckType() {
        return integrityCheckType;
    }

    public void setIntegrityCheckType(String integrityCheckType) {
        this.integrityCheckType = integrityCheckType;
    }

    public String getViewDocumentType() {
        return viewDocumentType;
    }

    public void setViewDocumentType(String viewDocumentType) {
        this.viewDocumentType = viewDocumentType;
    }

    public String getBindingProjectFormType() {
        return bindingProjectFormType;
    }

    public void setBindingProjectFormType(String bindingProjectFormType) {
        this.bindingProjectFormType = bindingProjectFormType;
    }

    public String getRelationFormType() {
        return relationFormType;
    }

    public void setRelationFormType(String relationFormType) {
        this.relationFormType = relationFormType;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(Integer deleteYn) {
        this.deleteYn = deleteYn;
    }

    public Integer getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(Integer nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public Integer getInStoreCtrl() {
        return inStoreCtrl;
    }

    public void setInStoreCtrl(Integer inStoreCtrl) {
        this.inStoreCtrl = inStoreCtrl;
    }

    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
    }

    public Long getPositionJobsId() {
        return positionJobsId;
    }

    public void setPositionJobsId(Long positionJobsId) {
        this.positionJobsId = positionJobsId;
    }

    public Long getSubprojectBindingId() {
        return subprojectBindingId;
    }

    public void setSubprojectBindingId(Long subprojectBindingId) {
        this.subprojectBindingId = subprojectBindingId;
    }

    @Override
    public String toString() {
        return "TaskNodeMainConfig{" +
                "id=" + id +
                ", deployId=" + deployId +
                ", taskId=" + taskId +
                ", taskName=" + taskName +
                ", progressTimeType=" + progressTimeType +
                ", nodeDeadline=" + nodeDeadline +
                ", deadlineWorkDayYn=" + deadlineWorkDayYn +
                ", nodeInstruction=" + nodeInstruction +
                ", assessYn=" + assessYn +
                ", subprojectYn=" + subprojectYn +
                ", gridGraphicsFusionPoint=" + gridGraphicsFusionPoint +
                ", preTaskId=" + preTaskId +
                ", integrityCheckType=" + integrityCheckType +
                ", viewDocumentType=" + viewDocumentType +
                ", bindingProjectFormType=" + bindingProjectFormType +
                ", relationFormType=" + relationFormType +
                ", createId=" + createId +
                ", createTime=" + createTime +
                ", updateId=" + updateId +
                ", updateTime=" + updateTime +
                ", deleteYn=" + deleteYn +
                ", nodeNumber=" + nodeNumber +
                ", inStoreCtrl=" + inStoreCtrl +
                ", nodeType=" + nodeType +
                ", positionJobsId=" + positionJobsId +
                ", subprojectBindingId=" + subprojectBindingId +
                "}";
    }
}
