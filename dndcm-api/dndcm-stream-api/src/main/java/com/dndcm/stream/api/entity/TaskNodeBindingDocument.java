package com.dndcm.stream.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 节点绑定文档信息表
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@TableName("task_node_binding_document")
@ApiModel(value="TaskNodeBindingDocument对象", description="节点绑定文档信息表 ")
public class TaskNodeBindingDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "节点id")
    private Long taskNodeMainConfigId;

    @ApiModelProperty(value = "任务id")
    private Long taskId;

    @ApiModelProperty(value = "流程部署id")
    private Long deployId;

    @ApiModelProperty(value = "文档名称")
    private String documentName;

    @ApiModelProperty(value = "归档类型（1-上传凭证；2-业务系统；3-来自图档库；4-来自规划）")
    private Integer archiveType;

    @ApiModelProperty(value = "图档库编码")
    private String imageFileLibraryCode;

    @ApiModelProperty(value = "归档说明")
    private String archiveInstruction;

    @ApiModelProperty(value = "是否写入图档库")
    private Integer wrietImageFileLibraryYn;

    @ApiModelProperty(value = "是否必须上传")
    private Integer mustUploadYn;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除")
    private int deleteYn;

    public int getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(int deleteYn) {
        this.deleteYn = deleteYn;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskNodeMainConfigId() {
        return taskNodeMainConfigId;
    }

    public void setTaskNodeMainConfigId(Long taskNodeMainConfigId) {
        this.taskNodeMainConfigId = taskNodeMainConfigId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getDeployId() {
        return deployId;
    }

    public void setDeployId(Long deployId) {
        this.deployId = deployId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Integer getArchiveType() {
        return archiveType;
    }

    public void setArchiveType(Integer archiveType) {
        this.archiveType = archiveType;
    }

    public String getImageFileLibraryCode() {
        return imageFileLibraryCode;
    }

    public void setImageFileLibraryCode(String imageFileLibraryCode) {
        this.imageFileLibraryCode = imageFileLibraryCode;
    }

    public String getArchiveInstruction() {
        return archiveInstruction;
    }

    public void setArchiveInstruction(String archiveInstruction) {
        this.archiveInstruction = archiveInstruction;
    }

    public Integer getWrietImageFileLibraryYn() {
        return wrietImageFileLibraryYn;
    }

    public void setWrietImageFileLibraryYn(Integer wrietImageFileLibraryYn) {
        this.wrietImageFileLibraryYn = wrietImageFileLibraryYn;
    }

    public Integer getMustUploadYn() {
        return mustUploadYn;
    }

    public void setMustUploadYn(Integer mustUploadYn) {
        this.mustUploadYn = mustUploadYn;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TaskNodeBindingDocument{" +
        "id=" + id +
        ", taskNodeMainConfigId=" + taskNodeMainConfigId +
        ", taskId=" + taskId +
        ", deployId=" + deployId +
        ", documentName=" + documentName +
        ", archiveType=" + archiveType +
        ", imageFileLibraryCode=" + imageFileLibraryCode +
        ", archiveInstruction=" + archiveInstruction +
        ", wrietImageFileLibraryYn=" + wrietImageFileLibraryYn +
        ", mustUploadYn=" + mustUploadYn +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", updateId=" + updateId +
        ", updateTime=" + updateTime +
        "}";
    }
}
