package com.dndcm.sys.api.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统用户vo
 * @author lyy
 * @date 2021-7-13 14:23:12
 */
@Data
public class SysUserVo {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "用户账号")
    private String userAccount;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "用户等级")
    private Integer userLevel;

    @ApiModelProperty(value = "用户部门")
    private String userDepartment;

    @ApiModelProperty(value = "用户状态")
    private Integer userStatus;
}
