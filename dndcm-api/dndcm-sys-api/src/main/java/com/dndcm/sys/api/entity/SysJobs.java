package com.dndcm.sys.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 岗位信息
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
@TableName("sys_jobs")
@ApiModel(value="SysJobs对象", description="岗位信息 ")
public class SysJobs implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "岗位编码")
    private String jobsCode;

    @ApiModelProperty(value = "岗位名称")
    private String jobsName;

    @ApiModelProperty(value = "岗位等级")
    private Integer jobsLevel;

    @ApiModelProperty(value = "状态")
    private Integer jobsStatus;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除")
    private int deleteYn;

    public int getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(int deleteYn) {
        this.deleteYn = deleteYn;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobsCode() {
        return jobsCode;
    }

    public void setJobsCode(String jobsCode) {
        this.jobsCode = jobsCode;
    }

    public String getJobsName() {
        return jobsName;
    }

    public void setJobsName(String jobsName) {
        this.jobsName = jobsName;
    }

    public Integer getJobsLevel() {
        return jobsLevel;
    }

    public void setJobsLevel(Integer jobsLevel) {
        this.jobsLevel = jobsLevel;
    }

    public Integer getJobsStatus() {
        return jobsStatus;
    }

    public void setJobsStatus(Integer jobsStatus) {
        this.jobsStatus = jobsStatus;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "SysJobs{" +
        "id=" + id +
        ", jobsCode=" + jobsCode +
        ", jobsName=" + jobsName +
        ", jobsLevel=" + jobsLevel +
        ", jobsStatus=" + jobsStatus +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", updateId=" + updateId +
        ", updateTime=" + updateTime +
        "}";
    }
}
