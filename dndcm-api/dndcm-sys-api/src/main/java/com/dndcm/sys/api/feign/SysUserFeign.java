package com.dndcm.sys.api.feign;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.vo.SysLoginOutput;
import com.dndcm.sys.api.entity.vo.SysUserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 系统管理业务层接口
 * @author lyy
 * @date 2021-7-14 16:28:02
 */
@FeignClient(name = "dndcm-sys")
public interface SysUserFeign {

    @RequestMapping("/sysUser/query")
    CommonResponse<SysUserVo> loadUserByUserName (@RequestBody SysUser user);

    @RequestMapping("/sysUser/query")
    CommonResponse<List<String>> getJobs(@RequestBody SysUser user);
}
