package com.dndcm.sys.api.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author lyy
 * @description
 * @date 2021-7-14 09:49:08
 **/
@Data
public class SysLoginOutput {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "账号")
    private String userAccount;

    @ApiModelProperty(value = "名字")
    private String userName;

    @ApiModelProperty(value = "用户级别")
    private Integer userLevel;

    @ApiModelProperty(value = "状态(字典)")
    private Integer status;

    @ApiModelProperty(value = "用户登录的token")
    private String token;
}
