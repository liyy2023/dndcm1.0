package com.dndcm.project.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目信息主表
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@TableName("project")
@ApiModel(value="Project对象", description="项目信息主表 ")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "项目类别id")
    private Long projectCategoryId;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "项目编码")
    private String projectCode;

    @ApiModelProperty(value = "项目状态(1-在途项目；2-规划库；3-储备库；4-计划库；5-归档库)")
    private Integer projectType;

    @ApiModelProperty(value = "用电地址")
    private String electricityAddress;

    @ApiModelProperty(value = "建设规模")
    private String constructionScale;

    @ApiModelProperty(value = "是否有开关站")
    private Integer switchStationYn;

    @ApiModelProperty(value = "年度")
    private Integer annual;

    @ApiModelProperty(value = "项目标签")
    private String projectTag;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除")
    private int deleteYn;

    public int getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(int deleteYn) {
        this.deleteYn = deleteYn;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getProjectCategoryId() {
        return projectCategoryId;
    }

    public void setProjectCategoryId(Long projectCategoryId) {
        this.projectCategoryId = projectCategoryId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Integer getProjectType() {
        return projectType;
    }

    public void setProjectType(Integer projectType) {
        this.projectType = projectType;
    }

    public String getElectricityAddress() {
        return electricityAddress;
    }

    public void setElectricityAddress(String electricityAddress) {
        this.electricityAddress = electricityAddress;
    }

    public String getConstructionScale() {
        return constructionScale;
    }

    public void setConstructionScale(String constructionScale) {
        this.constructionScale = constructionScale;
    }

    public Integer getSwitchStationYn() {
        return switchStationYn;
    }

    public void setSwitchStationYn(Integer switchStationYn) {
        this.switchStationYn = switchStationYn;
    }

    public Integer getAnnual() {
        return annual;
    }

    public void setAnnual(Integer annual) {
        this.annual = annual;
    }

    public String getProjectTag() {
        return projectTag;
    }

    public void setProjectTag(String projectTag) {
        this.projectTag = projectTag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Project{" +
        "id=" + id +
        ", region=" + region +
        ", projectCategoryId=" + projectCategoryId +
        ", projectName=" + projectName +
        ", projectCode=" + projectCode +
        ", projectType=" + projectType +
        ", electricityAddress=" + electricityAddress +
        ", constructionScale=" + constructionScale +
        ", switchStationYn=" + switchStationYn +
        ", annual=" + annual +
        ", projectTag=" + projectTag +
        ", remark=" + remark +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", updateId=" + updateId +
        ", updateTime=" + updateTime +
        "}";
    }
}
