package com.dndcm.project.api.feign;

import com.dndcm.project.api.Work;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 项目管理业务层接口
 * @author lyy
 * @date 2021-7-6 17:32:58
 */
@FeignClient(name = "dndcm-project")
public interface WorkFeign {
    /**
     * work
     * @param work
     * @return
     */
    @PostMapping("/work/getWork")
    Work getWork(Work work);
}
