package com.dndcm.project.api.entity.vo;

import com.dndcm.project.api.entity.Project;
import com.dndcm.stream.api.entity.ProjectProcessDeployment;
import com.dndcm.sys.api.entity.SysUserJobs;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProjectWait {
    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "项目编码")
    private String projectCode;

    @ApiModelProperty(value = "节点名称")
    private String taskName;

   /* @ApiModelProperty(value = "节点期限")
    private String node_deadline;

    @ApiModelProperty(value = "修改时间")
    private String updateTime;*/

}



