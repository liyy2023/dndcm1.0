package com.dndcm.project.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 凭证目录
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@TableName("certificate_directory")
@ApiModel(value="CertificateDirectory对象", description="凭证目录 ")
public class CertificateDirectory implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "流程实例id")
    private Long processInstanceId;

    @ApiModelProperty(value = "流程部署id")
    private Long deployId;

    @ApiModelProperty(value = "任务id")
    private Long taskId;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "文档名称")
    private String documentName;

    @ApiModelProperty(value = "绑定文档id")
    private Long bindingDocumentId;

    @ApiModelProperty(value = "归档确认人")
    private String archiveBy;

    @ApiModelProperty(value = "归档日期")
    private LocalDateTime archiveTime;

    @ApiModelProperty(value = "归档方式(1-上传凭证)")
    private Integer archiveType;

    @ApiModelProperty(value = "操作栏（文件路径）")
    private String fileUrl;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除")
    private int deleteYn;

    @ApiModelProperty(value = "上传文件")
    private MultipartFile file;

    @ApiModelProperty(value = "桶名")
    private String bucketName;

    public int getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(int deleteYn) {
        this.deleteYn = deleteYn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(Long processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public Long getDeployId() {
        return deployId;
    }

    public void setDeployId(Long deployId) {
        this.deployId = deployId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Long getBindingDocumentId() {
        return bindingDocumentId;
    }

    public void setBindingDocumentId(Long bindingDocumentId) {
        this.bindingDocumentId = bindingDocumentId;
    }

    public String getArchiveBy() {
        return archiveBy;
    }

    public void setArchiveBy(String archiveBy) {
        this.archiveBy = archiveBy;
    }

    public LocalDateTime getArchiveTime() {
        return archiveTime;
    }

    public void setArchiveTime(LocalDateTime archiveTime) {
        this.archiveTime = archiveTime;
    }

    public Integer getArchiveType() {
        return archiveType;
    }

    public void setArchiveType(Integer archiveType) {
        this.archiveType = archiveType;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    @Override
    public String toString() {
        return "CertificateDirectory{" +
        "id=" + id +
        ", processInstanceId=" + processInstanceId +
        ", deployId=" + deployId +
        ", taskId=" + taskId +
        ", taskName=" + taskName +
        ", documentName=" + documentName +
        ", bindingDocumentId=" + bindingDocumentId +
        ", archiveBy=" + archiveBy +
        ", archiveTime=" + archiveTime +
        ", archiveType=" + archiveType +
        ", fileUrl=" + fileUrl +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", updateId=" + updateId +
        ", updateTime=" + updateTime +
        "}";
    }
}
