package com.dndcm.project.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目类别信息表
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@TableName("project_category")
@ApiModel(value="ProjectCategory对象", description="项目类别信息表 ")
public class ProjectCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "类别名称")
    private String categoryName;

    @ApiModelProperty(value = "父类别id")
    private String parentId;

    @ApiModelProperty(value = "分管领导")
    private Long chargeLeader;

    @ApiModelProperty(value = "流程绑定")
    private Long deployId;

    @ApiModelProperty(value = "类别说明")
    private String categoryInstruction;

    @ApiModelProperty(value = "是否启用(1-启用，0-停用)")
    private Integer status;

    @ApiModelProperty(value = "是否在分类导航可见(1-是，0-否)")
    private Integer showYn;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除")
    private int deleteYn;

    public int getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(int deleteYn) {
        this.deleteYn = deleteYn;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getChargeLeader() {
        return chargeLeader;
    }

    public void setChargeLeader(Long chargeLeader) {
        this.chargeLeader = chargeLeader;
    }

    public Long getDeployId() {
        return deployId;
    }

    public void setDeployId(Long deployId) {
        this.deployId = deployId;
    }

    public String getCategoryInstruction() {
        return categoryInstruction;
    }

    public void setCategoryInstruction(String categoryInstruction) {
        this.categoryInstruction = categoryInstruction;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getShowYn() {
        return showYn;
    }

    public void setShowYn(Integer showYn) {
        this.showYn = showYn;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ProjectCategory{" +
        "id=" + id +
        ", categoryName=" + categoryName +
        ", chargeLeader=" + chargeLeader +
        ", deployId=" + deployId +
        ", categoryInstruction=" + categoryInstruction +
        ", status=" + status +
        ", showYn=" + showYn +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", updateId=" + updateId +
        ", updateTime=" + updateTime +
        "}";
    }
}
