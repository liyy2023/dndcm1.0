package com.dndcm.project.api;

import lombok.Data;

@Data
public class Work {
    private String name;

    private String description;
}
