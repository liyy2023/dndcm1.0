package com.dndcm.project.api.entity.vo;

import com.dndcm.project.api.entity.Project;
import com.dndcm.stream.api.entity.TaskProcedure;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 项目类型树
 * @author lyy
 * @date 2021-7-9 18:20:53
 */
@Data
public class ProjectCategoryTree {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "类别名称")
    private String categoryName;

    @ApiModelProperty(value = "父类别id")
    private Long parentId;

    @ApiModelProperty(value = "绑定流程id")
    private TaskProcedure deployId;

    @ApiModelProperty(value = "绑定流程name")
    private TaskProcedure deployName;

    @ApiModelProperty(value = "绑定流程类别")
    private Integer procedureType;

    @ApiModelProperty(value = "项目")
    private List<Project> projects;

    @ApiModelProperty(value = "子节点")
    private List<ProjectCategoryTree> children;

    @ApiModelProperty(value = "项目数量")
    private Integer projectNum;



}
