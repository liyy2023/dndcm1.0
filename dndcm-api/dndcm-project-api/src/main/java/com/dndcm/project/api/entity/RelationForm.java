package com.dndcm.project.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 关联表单
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@TableName("relation_form")
@ApiModel(value="RelationForm对象", description="关联表单 ")
public class RelationForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "任务配置id")
    private Long taskNodeMainConfigId;

    @ApiModelProperty(value = "任务id")
    private Long taskId;

    @ApiModelProperty(value = "所属阶段（1-规划；2-项目前期；3-施工前期；4-工程实施；5-竣工转资）")
    private Integer belongPhaseType;

    @ApiModelProperty(value = "配合营商环境（1-申请和工程设计环节；2-行政审批环节；3-外线施工环节）")
    private Integer cooperateBusinessEnvironment;

    @ApiModelProperty(value = "可设计委托")
    private Integer designableCommissionYn;

    @ApiModelProperty(value = "入库控制门(0-无；1-入规划库；2-入储备库；3-入计划库)")
    private Integer entryControlDoorType;

    @ApiModelProperty(value = "投资形象进度(0-无；1-累计一；2-累计二；3-累计三；4-累计四)")
    private Integer investmentImageProgress;

    @ApiModelProperty(value = "预算形象进度（0-无；1-累计一；2-累计二；3-累计三；4-累计四）")
    private Integer budgetImageProgress;

    @ApiModelProperty(value = "安全监督点")
    private Integer safetySupervisionYn;

    @ApiModelProperty(value = "安全监督分值")
    private Integer safetySupervisionScore;

    @ApiModelProperty(value = "安监评分说明")
    private String safetySupervisionIsntruction;

    @ApiModelProperty(value = "协同监督点")
    private Integer coordinatedSupervisionYn;

    @ApiModelProperty(value = "协同监督分值")
    private Integer coordinatedSupervisionScore;

    @ApiModelProperty(value = "协同监督说明")
    private String coordinatedSupervisionInstruction;

    @ApiModelProperty(value = "前置跳转条件")
    private String preJumpConditions;

    @ApiModelProperty(value = "归档条件")
    private String archiveCondition;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人")
    private Long updateId;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否删除")
    private int deleteYn;

    public int getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(int deleteYn) {
        this.deleteYn = deleteYn;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskNodeMainConfigId() {
        return taskNodeMainConfigId;
    }

    public void setTaskNodeMainConfigId(Long taskNodeMainConfigId) {
        this.taskNodeMainConfigId = taskNodeMainConfigId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getBelongPhaseType() {
        return belongPhaseType;
    }

    public void setBelongPhaseType(Integer belongPhaseType) {
        this.belongPhaseType = belongPhaseType;
    }

    public Integer getCooperateBusinessEnvironment() {
        return cooperateBusinessEnvironment;
    }

    public void setCooperateBusinessEnvironment(Integer cooperateBusinessEnvironment) {
        this.cooperateBusinessEnvironment = cooperateBusinessEnvironment;
    }

    public Integer getDesignableCommissionYn() {
        return designableCommissionYn;
    }

    public void setDesignableCommissionYn(Integer designableCommissionYn) {
        this.designableCommissionYn = designableCommissionYn;
    }

    public Integer getEntryControlDoorType() {
        return entryControlDoorType;
    }

    public void setEntryControlDoorType(Integer entryControlDoorType) {
        this.entryControlDoorType = entryControlDoorType;
    }

    public Integer getInvestmentImageProgress() {
        return investmentImageProgress;
    }

    public void setInvestmentImageProgress(Integer investmentImageProgress) {
        this.investmentImageProgress = investmentImageProgress;
    }

    public Integer getBudgetImageProgress() {
        return budgetImageProgress;
    }

    public void setBudgetImageProgress(Integer budgetImageProgress) {
        this.budgetImageProgress = budgetImageProgress;
    }

    public Integer getSafetySupervisionYn() {
        return safetySupervisionYn;
    }

    public void setSafetySupervisionYn(Integer safetySupervisionYn) {
        this.safetySupervisionYn = safetySupervisionYn;
    }

    public Integer getSafetySupervisionScore() {
        return safetySupervisionScore;
    }

    public void setSafetySupervisionScore(Integer safetySupervisionScore) {
        this.safetySupervisionScore = safetySupervisionScore;
    }

    public String getSafetySupervisionIsntruction() {
        return safetySupervisionIsntruction;
    }

    public void setSafetySupervisionIsntruction(String safetySupervisionIsntruction) {
        this.safetySupervisionIsntruction = safetySupervisionIsntruction;
    }

    public Integer getCoordinatedSupervisionYn() {
        return coordinatedSupervisionYn;
    }

    public void setCoordinatedSupervisionYn(Integer coordinatedSupervisionYn) {
        this.coordinatedSupervisionYn = coordinatedSupervisionYn;
    }

    public Integer getCoordinatedSupervisionScore() {
        return coordinatedSupervisionScore;
    }

    public void setCoordinatedSupervisionScore(Integer coordinatedSupervisionScore) {
        this.coordinatedSupervisionScore = coordinatedSupervisionScore;
    }

    public String getCoordinatedSupervisionInstruction() {
        return coordinatedSupervisionInstruction;
    }

    public void setCoordinatedSupervisionInstruction(String coordinatedSupervisionInstruction) {
        this.coordinatedSupervisionInstruction = coordinatedSupervisionInstruction;
    }

    public String getPreJumpConditions() {
        return preJumpConditions;
    }

    public void setPreJumpConditions(String preJumpConditions) {
        this.preJumpConditions = preJumpConditions;
    }

    public String getArchiveCondition() {
        return archiveCondition;
    }

    public void setArchiveCondition(String archiveCondition) {
        this.archiveCondition = archiveCondition;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "RelationForm{" +
        "id=" + id +
        ", taskNodeMainConfigId=" + taskNodeMainConfigId +
        ", taskId=" + taskId +
        ", belongPhaseType=" + belongPhaseType +
        ", cooperateBusinessEnvironment=" + cooperateBusinessEnvironment +
        ", designableCommissionYn=" + designableCommissionYn +
        ", entryControlDoorType=" + entryControlDoorType +
        ", investmentImageProgress=" + investmentImageProgress +
        ", budgetImageProgress=" + budgetImageProgress +
        ", safetySupervisionYn=" + safetySupervisionYn +
        ", safetySupervisionScore=" + safetySupervisionScore +
        ", safetySupervisionIsntruction=" + safetySupervisionIsntruction +
        ", coordinatedSupervisionYn=" + coordinatedSupervisionYn +
        ", coordinatedSupervisionScore=" + coordinatedSupervisionScore +
        ", coordinatedSupervisionInstruction=" + coordinatedSupervisionInstruction +
        ", preJumpConditions=" + preJumpConditions +
        ", archiveCondition=" + archiveCondition +
        ", createId=" + createId +
        ", createTime=" + createTime +
        ", updateId=" + updateId +
        ", updateTime=" + updateTime +
        "}";
    }
}
