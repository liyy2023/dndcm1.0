package com.dndcm.file.api.feign;

import com.dndcm.file.api.entity.FileOutput;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@FeignClient(name = "dndcm-file")
public interface FileFeign {

    @PostMapping("/file/upload")
    FileOutput uploadFile1(@RequestParam("file") MultipartFile file, @RequestParam("bucketName") String bucketName);

    @GetMapping("/file/download/{objectName}")
    ResponseEntity<byte[]> downloadToLocal(@PathVariable("objectName") String objectName, HttpServletResponse response);
}
