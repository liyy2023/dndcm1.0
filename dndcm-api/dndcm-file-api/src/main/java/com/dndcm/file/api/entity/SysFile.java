package com.dndcm.file.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 文件系统表
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-13
 */
@TableName("sys_file")
@ApiModel(value="SysFile对象", description="文件系统表")
public class SysFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "文件浏览/播放路径")
    private String viewUrl;

    @ApiModelProperty(value = "文件下载地址")
    private String downloadUrl;

    @ApiModelProperty(value = "桶名称")
    private String bucketName;

    @ApiModelProperty(value = "上传文件名称")
    private String oldFileName;

    @ApiModelProperty(value = "新文件名")
    private String newFileName;

    @ApiModelProperty(value = "文件类型：非枚举类型")
    private String contentYpe;

    @ApiModelProperty(value = "文件大分类：1-图片；2-音乐；3-视频")
    private Integer fileType;

    @ApiModelProperty(value = "是否删除")
    private Integer deleteYn;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人")
    private Long createId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "备注信息")
    private String remake;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getViewUrl() {
        return viewUrl;
    }

    public void setViewUrl(String viewUrl) {
        this.viewUrl = viewUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getOldFileName() {
        return oldFileName;
    }

    public void setOldFileName(String oldFileName) {
        this.oldFileName = oldFileName;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getContentYpe() {
        return contentYpe;
    }

    public void setContentYpe(String contentYpe) {
        this.contentYpe = contentYpe;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public Integer getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(Integer deleteYn) {
        this.deleteYn = deleteYn;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    @Override
    public String toString() {
        return "SysFile{" +
        "id=" + id +
        ", viewUrl=" + viewUrl +
        ", downloadUrl=" + downloadUrl +
        ", bucketName=" + bucketName +
        ", oldFileName=" + oldFileName +
        ", newFileName=" + newFileName +
        ", contentYpe=" + contentYpe +
        ", fileType=" + fileType +
        ", deleteYn=" + deleteYn +
        ", createTime=" + createTime +
        ", createId=" + createId +
        ", updateTime=" + updateTime +
        ", remake=" + remake +
        "}";
    }
}
