package com.dndcm.file.api.entity;

import lombok.Data;

@Data
public class FileOutput {
    //文件临时访问路径
    private String viewUrl;
    //上传文件的名称
    private String oldFileName;
    //存储空间生成的新文件名
    private String newFileName;
    //文件类型
    private String contentType;
}
