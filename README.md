##  配电网控制管理系统(dndcm1.0)

#### 项目介绍

#### 项目架构
##### 系统架构图


##### 微服务划分图


##### 模块说明
```text
|-- dndcm1.0
    |-- code-generator -- Mybatise-plus代码生成器
    |-- dndcm-api -- 各个微服务的API接口
    |   |-- dndcm-stream-api
    |   |-- dndcm-project-api
    |   |-- dndcm-sys-api
        |-- dndcm-file-api
    |-- dndcm-eureka -- 注册中心启动模块
    |-- dndcm-file -- 文件系统模块
    |-- dndcm-stream -- 流程微服务
    |-- dndcm-common -- 通用工具类
    |-- dndcm-project -- 项目微服务
    |-- dndcm-sys -- 系统微服务
    |-- dndcm-gateway -- 网关微服务

```
#### 技术选型
##### 后端技术
技术|说明
---|---
Spring Boot|容器+MVC框架
MyBatis Plus|MyBatis增强工具
MySql|数据库
Redis|分布式缓存
Elasticsearch|搜索引擎
Lombok|简化对象封装工具
Nginx|反向代理、限流、负载均衡、容错
Docker|虚拟化容器技术
Spring Cloud Gateway|API 网关
Spring Cloud Security+Oauth2|安全认证授权、第三方登录
Spring Cloud OpenFeign|服务消费（远程调用）
Spring Cloud Ribbon|服务消费（负载均衡）
Spring Cloud Netflix Eureka|注册中心
esdk-obs-java OBS|华为云OBS对象存储服务


##### 前端技术
技术|说明
---|---
Vue|前端框架
Vue-router|路由管理器
Element UI|前端UI框架
Axios|前端HTTP框架
V-Charts|前端图表框架
HTML CSS JS|前端技术
ECMAScript 6|JavaScript语言标准
JQuery|JS插件库
Thymeleaf|模板引擎

#### 环境搭建
##### 开发环境
工具|版本号
---|---
JDK|1.8
MySql|5.7.22
Redis|5.0
Elasticsearch|7.4.2
Nginx|1.17.10
Docker|19.03.05

##### 开发工具
工具|说明
---|---
IDEA|Java代码编译环境
VsCode|前端代码编辑器
VMware|虚拟机管理
Navicat|数据库可视化工具
Postman|接口调试工具
Xshell|Linux远程连接工具
Xftp|数据传输工具

##### 搭建步骤

#### 效果演示
##### 后台管理系统
​	**Mysql数据库：**
######主机地址：10.1.130.59:39316/dndcm
######用户：devusr02
######密码：devusr02_123

​	**测试账号：**

​	登录名：admin123		密码：123456

- **登录页**
  




####FAQ
- 代码生成器生成的entity需要手动转移到api中，vo和dto等实体类的定义都要在api中
- 拉下代码后一律切到dev分支开发，切忌直接在master开发
#### 开发维护
- **[Email](liyy2023@qq.com)**

- **[phone](18302412014)**



