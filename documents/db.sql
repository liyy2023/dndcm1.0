
DROP TABLE IF EXISTS task_node_main_config;
CREATE TABLE task_node_main_config(
                                      id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                                      deploy_id BIGINT    COMMENT '流程定义id' ,
                                      task_id BIGINT    COMMENT '任务id' ,
                                      task_name VARCHAR(128)    COMMENT '任务名称' ,
                                      progress_time_type int(2)    COMMENT '进度时限类型（1-周期；2-里程碑）' ,
                                      node_deadline INT    COMMENT '节点期限' ,
                                      deadline_work_day_yn VARCHAR(128)    COMMENT '时限只取工作日(1-是；0-否)' ,
                                      node_instruction VARCHAR(1024)    COMMENT '节点说明' ,
                                      assess_yn int(2)    COMMENT '是否考核（1-是；0-否）' ,
                                      subproject_yn int(2)    COMMENT '是否子工程（1-是；0-否）' ,
                                      grid_graphics_fusion_point VARCHAR(128)    COMMENT '电网图形融合点(1-无；2-方案编制；3-方案查看)' ,
                                      pre_task_id VARCHAR(1024)    COMMENT '前置节点（可能会有多个，用英文逗号间隔）' ,
                                      integrity_check_type VARCHAR(1024)    COMMENT '完整性检查' ,
                                      view_document_type VARCHAR(1024)    COMMENT '查看文档绑定' ,
                                      binding_project_form_type VARCHAR(1024)    COMMENT '绑定工程表单' ,
                                      relation_form_type VARCHAR(1024)    COMMENT '关联表单' ,
                                      create_id BIGINT    COMMENT '创建人' ,
                                      create_time DATETIME    COMMENT '创建时间' ,
                                      update_id BIGINT    COMMENT '更新人' ,
                                      update_time DATETIME    COMMENT '更新时间' ,
                                      delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                                      PRIMARY KEY (id)
) COMMENT = '任务节点配置表 ';

DROP TABLE IF EXISTS relation_form;
CREATE TABLE relation_form(
                              id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                              task_node_main_config_id BIGINT    COMMENT '任务配置id' ,
                              task_id BIGINT    COMMENT '任务id' ,
                              belong_phase_type int(2)    COMMENT '所属阶段（1-规划；2-项目前期；3-施工前期；4-工程实施；5-竣工转资）' ,
                              cooperate_business_environment int(2)    COMMENT '配合营商环境（1-申请和工程设计环节；2-行政审批环节；3-外线施工环节）' ,
                              designable_commission_yn int(2)    COMMENT '可设计委托' ,
                              entry_control_door_type int(2)    COMMENT '入库控制门(0-无；1-入规划库；2-入储备库；3-入计划库)' ,
                              investment_image_progress int(2)    COMMENT '投资形象进度(0-无；1-累计一；2-累计二；3-累计三；4-累计四)' ,
                              budget_image_progress int(2)    COMMENT '预算形象进度（0-无；1-累计一；2-累计二；3-累计三；4-累计四）' ,
                              safety_supervision_yn int(2)    COMMENT '安全监督点' ,
                              safety_supervision_score INT    COMMENT '安全监督分值' ,
                              safety_supervision_isntruction VARCHAR(1024)    COMMENT '安监评分说明' ,
                              coordinated_supervision_yn int(2)    COMMENT '协同监督点' ,
                              coordinated_supervision_score INT    COMMENT '协同监督分值' ,
                              coordinated_supervision_instruction VARCHAR(128)    COMMENT '协同监督说明' ,
                              pre_jump_conditions VARCHAR(128)    COMMENT '前置跳转条件' ,
                              archive_condition VARCHAR(128)    COMMENT '归档条件' ,
                              create_id BIGINT    COMMENT '创建人' ,
                              create_time DATETIME    COMMENT '创建时间' ,
                              update_id BIGINT    COMMENT '更新人' ,
                              update_time DATETIME    COMMENT '更新时间' ,
                              delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                              PRIMARY KEY (id)
) COMMENT = '关联表单 ';

DROP TABLE IF EXISTS task_permission;
CREATE TABLE task_permission(
                                id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                                permission_code VARCHAR(128)    COMMENT '权限编码' ,
                                permission_name VARCHAR(128)    COMMENT '权限名称' ,
                                permission_type int(2)    COMMENT '权限类型（1-完整性检查；2-查看文档绑定；3-绑定工程表单）' ,
                                create_id BIGINT    COMMENT '创建人' ,
                                create_time DATETIME    COMMENT '创建时间' ,
                                update_id BIGINT    COMMENT '更新人' ,
                                update_time DATETIME    COMMENT '更新时间' ,
                                delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                                PRIMARY KEY (id)
) COMMENT = '任务权限列表 ';

DROP TABLE IF EXISTS task_node_binding_document;
CREATE TABLE task_node_binding_document(
                                           id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                                           task_node_main_config_id BIGINT    COMMENT '节点id' ,
                                           task_id BIGINT    COMMENT '任务id' ,
                                           deploy_id BIGINT    COMMENT '流程部署id' ,
                                           document_name VARCHAR(128)    COMMENT '文档名称' ,
                                           archive_type int(2)    COMMENT '归档类型（1-上传凭证；2-业务系统；3-来自图档库；4-来自规划）' ,
                                           image_file_library_code VARCHAR(128)    COMMENT '图档库编码' ,
                                           archive_instruction VARCHAR(1024)    COMMENT '归档说明' ,
                                           wriet_image_file_library_yn int(2)    COMMENT '是否写入图档库' ,
                                           must_upload_yn int(2)    COMMENT '是否必须上传' ,
                                           create_id BIGINT    COMMENT '创建人' ,
                                           create_time DATETIME    COMMENT '创建时间' ,
                                           update_id BIGINT    COMMENT '更新人' ,
                                           update_time DATETIME    COMMENT '更新时间' ,
                                           delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                                           PRIMARY KEY (id)
) COMMENT = '节点绑定文档信息表 ';

DROP TABLE IF EXISTS task_node_permission;
CREATE TABLE task_node_permission(
                                     id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                                     config_id BIGINT    COMMENT '任务节点id' ,
                                     permission_id BIGINT    COMMENT '权限id' ,
                                     create_id BIGINT    COMMENT '创建人' ,
                                     create_time DATETIME    COMMENT '创建时间' ,
                                     update_id BIGINT    COMMENT '更新人' ,
                                     update_time DATETIME    COMMENT '更新时间' ,
                                     delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                                     PRIMARY KEY (id)
) COMMENT = '任务节点权限表 ';

DROP TABLE IF EXISTS task_procedure;
CREATE TABLE task_procedure(
                               id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                               project_id BIGINT    COMMENT '所属任务' ,
                               procedure_name VARCHAR(128)    COMMENT '流程名称' ,
                               procedure_type int(2)    COMMENT '流程类别' ,
                               description VARCHAR(128)    COMMENT '流程描述' ,
                               create_id BIGINT    COMMENT '创建人' ,
                               create_time DATETIME    COMMENT '创建时间' ,
                               update_id BIGINT    COMMENT '更新人' ,
                               update_time DATETIME    COMMENT '更新时间' ,
                               delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                               PRIMARY KEY (id)
) COMMENT = '任务流程表 ';

DROP TABLE IF EXISTS project;
CREATE TABLE project(
                        id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                        region VARCHAR(128)    COMMENT '地区' ,
                        project_category_id BIGINT    COMMENT '项目类别id' ,
                        project_name VARCHAR(1024)    COMMENT '项目名称' ,
                        project_code VARCHAR(128)    COMMENT '项目编码' ,
                        project_type int(2)    COMMENT '项目状态(1-在途项目；2-规划库；3-储备库；4-计划库；5-归档库)' ,
                        electricity_address VARCHAR(512)    COMMENT '用电地址' ,
                        construction_scale VARCHAR(1024)    COMMENT '建设规模' ,
                        switch_station_yn int(2)    COMMENT '是否有开关站' ,
                        annual INT    COMMENT '年度' ,
                        project_tag VARCHAR(1024)    COMMENT '项目标签' ,
                        remark VARCHAR(1024)    COMMENT '备注' ,
                        create_id BIGINT    COMMENT '创建人' ,
                        create_time DATETIME    COMMENT '创建时间' ,
                        update_id BIGINT    COMMENT '更新人' ,
                        update_time DATETIME    COMMENT '更新时间' ,
                        delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                        PRIMARY KEY (id)
) COMMENT = '项目信息主表 ';

DROP TABLE IF EXISTS certificate_directory;
CREATE TABLE certificate_directory(
                                      id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                                      process_instance_id BIGINT    COMMENT '流程实例id' ,
                                      deploy_id BIGINT    COMMENT '流程部署id' ,
                                      task_id BIGINT    COMMENT '任务id' ,
                                      task_name VARCHAR(128)    COMMENT '任务名称' ,
                                      document_name VARCHAR(128)    COMMENT '文档名称' ,
                                      binding_document_id BIGINT    COMMENT '绑定文档id' ,
                                      archive_by VARCHAR(128)    COMMENT '归档确认人' ,
                                      archive_time DATETIME    COMMENT '归档日期' ,
                                      archive_type int(2)    COMMENT '归档方式(1-上传凭证)' ,
                                      file_url VARCHAR(512)    COMMENT '操作栏（文件路径）' ,
                                      create_id BIGINT    COMMENT '创建人' ,
                                      create_time DATETIME    COMMENT '创建时间' ,
                                      update_id BIGINT    COMMENT '更新人' ,
                                      update_time DATETIME    COMMENT '更新时间' ,
                                      delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                                      PRIMARY KEY (id)
) COMMENT = '凭证目录 ';

DROP TABLE IF EXISTS business_list;
CREATE TABLE business_list(
                              id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                              process_instance_id BIGINT    COMMENT '流程实例id' ,
                              deploy_id BIGINT    COMMENT '流程部署id' ,
                              task_id BIGINT    COMMENT '任务id' ,
                              task_name VARCHAR(128)    COMMENT '任务名称' ,
                              table_name VARCHAR(128)    COMMENT '表名' ,
                              table_id BIGINT    COMMENT '表id' ,
                              create_id BIGINT    COMMENT '创建人' ,
                              create_time DATETIME    COMMENT '创建时间' ,
                              update_id BIGINT    COMMENT '更新人' ,
                              update_time DATETIME    COMMENT '更新时间' ,
                              delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                              PRIMARY KEY (id)
) COMMENT = '业务清单 ';

DROP TABLE IF EXISTS project_category;
CREATE TABLE project_category(
                                 id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                                 category_name VARCHAR(128)    COMMENT '类别名称' ,
                                 charge_leader BIGINT    COMMENT '分管领导' ,
                                 deploy_id BIGINT    COMMENT '流程绑定' ,
                                 category_instruction VARCHAR(1024)    COMMENT '类别说明' ,
                                 status int(2)    COMMENT '是否启用(1-启用，0-停用)' ,
                                 show_yn int(2)    COMMENT '是否在分类导航可见(1-是，0-否)' ,
                                 create_id BIGINT    COMMENT '创建人' ,
                                 create_time DATETIME    COMMENT '创建时间' ,
                                 update_id BIGINT    COMMENT '更新人' ,
                                 update_time DATETIME    COMMENT '更新时间' ,
                                 delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                                 PRIMARY KEY (id)
) COMMENT = '项目类别信息表 ';

DROP TABLE IF EXISTS sys_user;
CREATE TABLE sys_user(
                         id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                         user_account VARCHAR(128)    COMMENT '用户账号' ,
                         user_name VARCHAR(128)    COMMENT '用户名称' ,
                         user_level int(2)    COMMENT '用户等级' ,
                         user_department_id BIGINT    COMMENT '用户部门' ,
                         user_status int(2)    COMMENT '用户状态' ,
                         user_password VARCHAR(128)    COMMENT '密码' ,
                         create_id BIGINT    COMMENT '创建人' ,
                         create_time DATETIME    COMMENT '创建时间' ,
                         update_id BIGINT    COMMENT '更新人' ,
                         update_time DATETIME    COMMENT '更新时间' ,
                         delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                         PRIMARY KEY (id)
) COMMENT = '用户信息 ';

DROP TABLE IF EXISTS sys_department;
CREATE TABLE sys_department(
                               id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                               department_code VARCHAR(128)    COMMENT '部门编码' ,
                               department_name VARCHAR(128)    COMMENT '部门名称' ,
                               parent_id BIGINT    COMMENT '父级部门id' ,
                               department_level int(2)    COMMENT '部门等级' ,
                               department_instruction VARCHAR(512)    COMMENT '部门说明' ,
                               department_status int(2)    COMMENT '状态' ,
                               create_id BIGINT    COMMENT '创建人' ,
                               create_time DATETIME    COMMENT '创建时间' ,
                               update_id BIGINT    COMMENT '更新人' ,
                               update_time DATETIME    COMMENT '更新时间' ,
                               delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                               PRIMARY KEY (id)
) COMMENT = '部门信息表 ';

DROP TABLE IF EXISTS sys_jobs;
CREATE TABLE sys_jobs(
                         id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                         jobs_code VARCHAR(128)    COMMENT '岗位编码' ,
                         jobs_name VARCHAR(128)    COMMENT '岗位名称' ,
                         jobs_level int(2)    COMMENT '岗位等级' ,
                         jobs_status int(2)    COMMENT '状态' ,
                         create_id BIGINT    COMMENT '创建人' ,
                         create_time DATETIME    COMMENT '创建时间' ,
                         update_id BIGINT    COMMENT '更新人' ,
                         update_time DATETIME    COMMENT '更新时间' ,
                         delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                         PRIMARY KEY (id)
) COMMENT = '岗位信息 ';

DROP TABLE IF EXISTS sys_user_jobs;
CREATE TABLE sys_user_jobs(
                              id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                              user_id BIGINT    COMMENT '用户id' ,
                              jobs_id BIGINT    COMMENT '岗位id' ,
                              create_id BIGINT    COMMENT '创建人' ,
                              create_time DATETIME    COMMENT '创建时间' ,
                              update_id BIGINT    COMMENT '更新人' ,
                              update_time DATETIME    COMMENT '更新时间' ,
                              delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                              PRIMARY KEY (id)
) COMMENT = '用户关联岗位信息表 ';

DROP TABLE IF EXISTS sys_menu;
CREATE TABLE sys_menu(
                         id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                         menu_name VARCHAR(128)    COMMENT '菜单名称' ,
                         parent_id BIGINT    COMMENT '父级菜单id' ,
                         url VARCHAR(128)    COMMENT '菜单url' ,
                         type int(2)    COMMENT '类型（0-目录，1-菜单，2-按钮）' ,
                         order_num INT    COMMENT '排序' ,
                         menu_status int(2)    COMMENT '状态' ,
                         create_id BIGINT    COMMENT '创建人' ,
                         create_time DATETIME    COMMENT '创建时间' ,
                         update_id BIGINT    COMMENT '更新人' ,
                         update_time DATETIME    COMMENT '更新时间' ,
                         delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                         PRIMARY KEY (id)
) COMMENT = '菜单信息表 ';

DROP TABLE IF EXISTS sys_jobs_menu;
CREATE TABLE sys_jobs_menu(
                              id BIGINT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
                              jobs_id BIGINT    COMMENT '岗位id' ,
                              menu_id BIGINT    COMMENT '菜单id' ,
                              create_id BIGINT    COMMENT '创建人' ,
                              create_time DATETIME    COMMENT '创建时间' ,
                              update_id BIGINT    COMMENT '更新人' ,
                              update_time DATETIME    COMMENT '更新时间' ,
                              delete_yn int(2) COMMENT '是否删除（1-是，2否）' ,
                              PRIMARY KEY (id)
) COMMENT = '岗位菜单关系表 ';


