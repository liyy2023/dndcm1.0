package com.dndcm.file.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lyy
 * @description
 * @date 2021-5-7 14:56:31
 */
@ApiModel("文件上传出参")
@Data
public class FileOutput {

    @ApiModelProperty("文件临时访问路径")
    private String viewUrl;
    @ApiModelProperty(value = "上传文件的名称")
    private String oldFileName;
    @ApiModelProperty(value = "存储空间生成的新文件名")
    private String newFileName;
    @ApiModelProperty(value = "文件类型")
    private String contentType;
}
