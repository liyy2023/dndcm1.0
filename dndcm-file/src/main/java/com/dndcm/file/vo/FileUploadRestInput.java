package com.dndcm.file.vo;


import com.dndcm.commn.request.AbstractRequest;
import com.dndcm.commn.utils.AbstractParamUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author lyy
 * @description
 * @date 2021-5-7 14:55:54
 **/
@Data
public class FileUploadRestInput{
    @ApiModelProperty("文件")
    private MultipartFile file;
    @ApiModelProperty("桶名称[文件类型:视频：dndcm-dev-video ; 图片：dndcm-dev-image ; PDF: dndcm-dev-pdf ;]")
    private String bucketName;

    public void checkInput() {
        AbstractParamUtil.nonNull(file,"文件不能为空");
        AbstractParamUtil.notBlank(bucketName,"桶名称不能为空");
    }

}
