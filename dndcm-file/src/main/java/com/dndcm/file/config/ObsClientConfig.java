package com.dndcm.file.config;

import com.obs.services.ObsClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lyy
 * @description
 * @date 2021-5-7 15:36:15
 **/
@Slf4j
@Configuration
public class ObsClientConfig {

    @Value("${dndcm.obs.endPoint}")
    private String endPoint;

    @Value("${dndcm.obs.accessKey}")
    private String accessKey;

    @Value("${dndcm.obs.secretKey}")
    private String secretKey;

    @Bean
    @ConditionalOnProperty(prefix = "dndcm.obs", value = "enabled", matchIfMissing = false)
    public ObsClient obsClient(){
        //String accessKey, String secretKey, String endPoint
        log.info("spring容器加入ObsClient");
        return new ObsClient(accessKey,secretKey,endPoint);
    }
}
