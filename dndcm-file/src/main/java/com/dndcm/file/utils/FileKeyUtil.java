package com.dndcm.file.utils;



import com.dndcm.commn.utils.AbstractUuidUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * 文件上传获取 fileKey
 *
 * @author: liuY
 * @date: 2020/7/25 16:24
 */
public class FileKeyUtil {

    public static String getUuidFileKey(String bucketName,String contentType) {
        return bucketName + "+" + AbstractUuidUtil.uuidWithoutUnderscore() + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + "." +contentType;
    }
}
