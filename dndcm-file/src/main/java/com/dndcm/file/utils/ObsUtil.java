package com.dndcm.file.utils;

import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.HeaderResponse;
import com.obs.services.model.ObjectMetadata;
import com.obs.services.model.ObsObject;
import com.obs.services.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;

/**
 * 华为云obs工具类
 * @author lyy
 * @description
 * @date 2021-5-7 15:01:22
 **/
@Slf4j
@Component
public class ObsUtil {

    @Autowired
    private ObsClient obsClient;


    /**
     * 创建桶
     * @param bucketName
     * @return
     */
    public Boolean createBucket(String bucketName){
        try{
            // 创建桶成功
            HeaderResponse response = obsClient.createBucket(bucketName);
            log.info(response.getRequestId());
            log.info("创建桶成功");
            return Boolean.TRUE;
        }
        catch (ObsException e)
        {
            // 创建桶失败
            log.error("创建桶失败",e);
            log.error("HTTP Code: " + e.getResponseCode());
            log.error("Error Code:" + e.getErrorCode());
            log.error("Error Message: " + e.getErrorMessage());
            log.error("Request ID:" + e.getErrorRequestId());
            log.error("Host ID:" + e.getErrorHostId());
            return Boolean.FALSE;
        }
    }

    /**
     * 判断桶是否存在
     * @param bucketName
     * @return
     */
    public Boolean exists(String bucketName){
        return obsClient.headBucket(bucketName);
    }

    /**
     * 删除桶
     * @param bucketName
     * @return
     */
    public Boolean deleteBucket(String bucketName){
        try {
            obsClient.deleteBucket(bucketName);
            log.info("删除桶成功");
            return Boolean.TRUE;
        }catch (Exception e){
            log.error("删除桶失败",e);
            return Boolean.FALSE;
        }
    }

    /**
     * 文件上传
     * @param bucketName 桶名称
     * @param objectName 文件名称
     * @param stream 文件流
     * @param objectSize 文件大小
     * @param contentType 文件类型
     * @return 文件上传路径
     */
    public String putObject(String bucketName, String objectName, InputStream stream,Integer objectSize ,String contentType){
        try{
            ObjectMetadata metadata = new ObjectMetadata();
            if(objectSize != null && objectSize != 0){
                metadata.setContentLength((long) objectSize);
            }
            if(null != contentType && contentType != ""){
                metadata.setContentType(contentType);
            }
            PutObjectResult putObjectResult = obsClient.putObject(bucketName, objectName, stream,metadata);
            log.info("文件上传成功");
            return putObjectResult.getObjectUrl();
        }catch (Exception e){
            log.error("文件上传失败",e);
            return null;
        }
    }

    /**
     * 下载文件
     * @param bucketName
     * @param objectName
     * @return
     */
    public InputStream getObject(String bucketName, String objectName){
        try {
            ObsObject obsObject = obsClient.getObject(bucketName, objectName);
            log.info("文件读取成功");
            return obsObject.getObjectContent();
        }catch (Exception e){
            log.error("文件读取失败",e);
            return null;
        }
    }
}
