package com.dndcm.file;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@Slf4j
@SpringBootApplication(scanBasePackages = {
        "com.dndcm.file",
        "com.dndcm.file.service",
        "com.dndcm.commn.config"
})
@MapperScan(basePackages = "com.dndcm.file.mapper")
@EnableEurekaClient
@EnableFeignClients
public class FileApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class,args);
        log.info("文件管理微服务模块启动成功。。。");
        log.info("swagger地址:http://localhost:9096/dndcm-file/swagger-ui.html#/");
        log.info("访问地址:http://localhost:9096/dndcm-file");
    }
}
