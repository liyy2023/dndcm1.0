package com.dndcm.file.controller;


import com.dndcm.commn.response.CommonResponse;
import com.dndcm.file.service.SysFileService;
import com.dndcm.file.utils.ObsUtil;
import com.dndcm.file.vo.FileOutput;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lyy
 * @description 文件上传下载控制器
 * @date 2021-5-7 15:40:38
 */
@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    ObsUtil obsUtil;
    @Autowired
    SysFileService sysFileService;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public CommonResponse<FileOutput> uploadFile1(@RequestParam("file") MultipartFile file, @RequestParam("bucketName") String bucketName) throws IOException {
        return CommonResponse.ok(sysFileService.upload(file,bucketName));
    }

    @ApiOperation("文件下载")
    @GetMapping("/download/{objectName}")
    public ResponseEntity<byte[]> downloadToLocal(@PathVariable("objectName") String objectName, HttpServletResponse response) throws IOException{
        byte[] bytes = sysFileService.download(objectName);
        //设置header
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Accept-Ranges", "bytes");
        httpHeaders.add("Content-Length", bytes.length + "");
        //把文件名按UTF-8取出并按ISO8859-1编码，保证弹出窗口中的文件名中文不乱码，中文不要太多，最多支持17个中文，因为header有150个字节限制。
        objectName = new String(objectName.getBytes("UTF-8"), "ISO8859-1");
        httpHeaders.add("Content-disposition", "attachment; filename=" + objectName);
        httpHeaders.add("Content-Type", "text/plain;charset=utf-8");
        return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.CREATED);
    }
}
