package com.dndcm.file.service.impl;

import com.dndcm.commn.exception.DndcmException;
import com.dndcm.commn.exception.ResponseCodeImpl;
import com.dndcm.file.utils.FileKeyUtil;
import com.dndcm.file.utils.ObsUtil;
import com.dndcm.file.vo.FileOutput;
import com.dndcm.file.api.entity.SysFile;
import com.dndcm.file.mapper.SysFileMapper;
import com.dndcm.file.service.SysFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

/**
 * <p>
 * 文件系统表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-13
 */
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {

    @Autowired
    ObsUtil obsUtil;

    @Override
    public FileOutput upload(MultipartFile file, String bucketName) throws IOException {
        FileOutput output = new FileOutput();
        String filename = file.getOriginalFilename();
        // 获取文件后缀名
        String[] split = filename.split("\\.");
        String uuidFileKey = FileKeyUtil.getUuidFileKey(bucketName,split[split.length - 1]);
        String contentType = file.getContentType();
        long fileSize = file.getSize();
        if(!obsUtil.exists(bucketName)){
            obsUtil.createBucket(bucketName);
        }
        String url = obsUtil.putObject(bucketName, uuidFileKey, file.getInputStream(), (int) fileSize,contentType);
        output.setViewUrl(url);
        output.setNewFileName(uuidFileKey);
        output.setOldFileName(filename);
        output.setContentType(contentType);
        SysFile fileDo = new SysFile();
        // 设置文件信息
        fileDo.setNewFileName(uuidFileKey);
        fileDo.setOldFileName(filename);
        fileDo.setContentYpe(contentType);
        fileDo.setViewUrl(url);
        fileDo.setCreateTime(LocalDateTime.now());
        this.save(fileDo);
        return output;
    }

    @Override
    public byte[] download(String objectName) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        // 根据文件名解析桶名称
        if(StringUtils.isEmpty(objectName)){
            throw new DndcmException(new ResponseCodeImpl("9999900001","文件名不能为空"));
        }
        String[] split = objectName.split("\\+");
        String bucketName = split[0];
        InputStream stream = obsUtil.getObject(bucketName, objectName);
        //用于转换byte
        byte[] buffer = new byte[4096];
        int n = 0;
        while (-1 != (n = stream.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }
}
