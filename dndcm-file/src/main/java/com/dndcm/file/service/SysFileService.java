package com.dndcm.file.service;

import com.dndcm.file.vo.FileOutput;
import com.dndcm.file.api.entity.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 文件系统表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-13
 */
public interface SysFileService extends IService<SysFile> {

    /**
     * @param file
     * @param bucketName
     * @return
     * @description
     * @throws IOException
     */
    FileOutput upload(MultipartFile file, String bucketName) throws IOException;

    /**
     * @param objectName
     * @return
     * @description
     * @throws IOException
     */
    byte[] download(String objectName) throws IOException;

}
