package com.dndcm.file.mapper;

import com.dndcm.file.api.entity.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件系统表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-13
 */
@Mapper
public interface SysFileMapper extends BaseMapper<SysFile> {

}
