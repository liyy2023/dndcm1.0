package com.dndcm.commn.exception;

/**
 * @description: 自定义入参非法的异常 <br>
 * @date: 2021-7-9 14:35:44 <br>
 * @author: lyy <br>
 */
public class GlobalInvalidArgumentException extends RuntimeException {
    private static final long serialVersionUID = -1605515531880249974L;

    /**
     * 无参构造函数
     */
    public GlobalInvalidArgumentException() {
        super();
    }

    /**
     * 标识异常描述信息的构造函数
     *
     * @param message
     */
    public GlobalInvalidArgumentException(String message) {
        super(message);
    }

    /**
     * 包含异常描述信息和异常堆栈的构造函数
     *
     * @param message
     * @param cause
     */
    public GlobalInvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 包含异常堆栈的构造函数
     *
     * @param cause
     */
    public GlobalInvalidArgumentException(Throwable cause) {
        super(cause);
    }
}
