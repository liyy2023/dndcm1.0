package com.dndcm.commn.exception;


import com.dndcm.commn.constant.ResponseCode;

public class ResponseCodeImpl implements ResponseCode {
    private String name;
    private String code;
    private String message;
    private int args;

    public ResponseCodeImpl(String code, String message, int args) {
        this.code = code;
        this.message = message;
        this.args = args;
    }

    public ResponseCodeImpl(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseCodeImpl() {
    }
    @Override
    public String name() {
        return this.name;
    }
    @Override
    public String getCode() {
        return this.code;
    }
    @Override
    public String getMessage() {
        return this.message;
    }
    @Override
    public int getArgs() {
        return this.args;
    }
}
