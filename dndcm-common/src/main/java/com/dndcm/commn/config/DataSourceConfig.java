package com.dndcm.commn.config;

/**
 * 重写Datasource
 * @author liyy
 * @create 2021-6-23 11:12:01
 */

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.dndcm.commn.utils.DruidUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Druid数据源配置
 * @author lyy
 * @date 2021-6-23 11:18:27
 */
@Configuration
@PropertySource("classpath:application.yml")
public class DataSourceConfig {

    private static String dbUrl = "jdbc:mysql://10.1.130.59:39316/dndcm?serverTimezone=GMT&characterEncoding=utf-8";

    private static String username = "devusr02";

    private static String password = "CkwbnuhtXlUsgO6BNPgsCXldU/7gchb2D5HG0BJtKQzXwqP0k8EX82+WusOdZRJTqGJ3ZOwwptBmhAGQXk6soQ==";

    private static String driverClassName = "com.mysql.cj.jdbc.Driver";


    /**
     * 注册DruidServlet
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean druidServletRegistrationBean() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
        servletRegistrationBean.setServlet(new StatViewServlet());
        servletRegistrationBean.addUrlMappings("/druid/*");
        //登录查看信息的账号密码.
        servletRegistrationBean.addInitParameter("loginUsername", "admin");
        servletRegistrationBean.addInitParameter("loginPassword", "123456");
        return servletRegistrationBean;
    }

    /**
     * 注册DruidFilter拦截
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean druidFilterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        Map<String, String> initParams = new HashMap<String, String>();
        //设置忽略请求
        initParams.put("exclusions", "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*");
        filterRegistrationBean.setInitParameters(initParams);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    /**
     * 配置DataSource
     * @return
     * @throws SQLException
     */
    @Bean
    @Primary
    public DataSource dataSource() throws SQLException {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(username);
        try {
            druidDataSource.setPassword(DruidUtils.decrypt(password));
        } catch (Exception e) {
            e.printStackTrace();
        }
        druidDataSource.setUrl(dbUrl);
        druidDataSource.setFilters("stat,wall");
        druidDataSource.setUseGlobalDataSourceStat(true);
        druidDataSource.setDriverClassName(driverClassName);
        return druidDataSource;
    }

    public void setDbUrl(String dbUrl) {
        DataSourceConfig.dbUrl = "jdbc:mysql://10.1.130.59:39316/dndcm?serverTimezone=GMT&characterEncoding=utf-8";
    }

    public void setUsername(String username) {

        DataSourceConfig.username = "devusr02";
    }

    public void setPassword(String password) {
        DataSourceConfig.password = "CkwbnuhtXlUsgO6BNPgsCXldU/7gchb2D5HG0BJtKQzXwqP0k8EX82+WusOdZRJTqGJ3ZOwwptBmhAGQXk6soQ==";
    }

    public void setDriverClassName(String driverClassName) {
        DataSourceConfig.driverClassName = "com.mysql.cj.jdbc.Driver";
    }

}
