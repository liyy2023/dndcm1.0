package com.dndcm.commn.request;

import com.dndcm.commn.base.PageInput;
import com.dndcm.commn.utils.AbstractParamUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: 分页查询请求基类 <br>
 * @date: 2021-7-9 14:22:18 <br>
 * @author: lyy <br>
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractPageRequest  {
    private static final long serialVersionUID = -98424552L;

    @ApiModelProperty(value = "分页方式")
    private PageInput pageType;

    public boolean isWrite() {
        return false;
    }


    public void checkInput() {
        AbstractParamUtil.nonNull(pageType, "分页方式不能为空");
        pageType.checkInput();
    }
}
