package com.dndcm.commn.request;

import com.dndcm.commn.base.Transferable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @description 所有接口请求基类
 * @author liyy
 * @date 2021-7-9 14:25:30
 * @param:
 **/
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractRequest implements Transferable {
    private static final long serialVersionUID = -677923676L;

    @ApiModelProperty(value = "请求消息唯一标识，由前端或API网关生成以便实现跟踪和幂等")
    private String uuid;


    @ApiModelProperty(hidden = true, value = "客户端类型:1-app;2-web;3-pc", required = true)
    private String clientType;

    @ApiModelProperty(hidden = true, value = "客户端IP", required = true)
    private String clientIp;

    @ApiModelProperty(hidden = true, value = "用户类型1-普通用户;2-租户员工；3-后台管理用户；4-游客", required = false)
    private String userType;

    @ApiModelProperty(hidden = true, value = "用户编号", required = false)
    private String userId;

    @ApiModelProperty(hidden = true, value = "登录账号，登录时所使用的账号名")
    private String account;

    @ApiModelProperty(hidden = true, value = "请求类名，每个请求需要定义一个唯一的类名")
    public String getClassName() {
        return getClass().getName();
    }

    @ApiModelProperty(hidden = true, value = "是否为写请求，用于区分CQRS读写操作")
    public abstract boolean isWrite();

    @Override
    public String toString() {
        return excludeEmptyFieldsJson();
    }

    public abstract void checkInput();

}
