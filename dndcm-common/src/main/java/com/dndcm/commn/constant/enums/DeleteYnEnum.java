package com.dndcm.commn.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DeleteYnEnum {
    YES(1,"是"),
    NO(0,"否");

    private Integer code;

    private String desc;

}
