package com.dndcm.commn.base;

import com.dndcm.commn.utils.AbstractJsonUtil;

import java.io.Serializable;

/**
 * @author lyy
 * @description: 数据传输接口
 * @date 2021-7-9 14:32:12
 * @param:
 */
public interface Transferable extends Serializable {

    /**
     * @return 不含取值为null、空字符串、空集合的字段且带缩进的JSON字符串
     */
    default String excludeEmptyFieldsJson() {
        return AbstractJsonUtil.getIndentNonEmptyJsonString(this);
    }

    /**
     * @return 包含取值为null、空字符串、空集合的字段且带缩进的JSON字符串
     */
    default String includeEmptyFieldsJson() {
        return AbstractJsonUtil.getIndentJsonString(this);
    }
}
