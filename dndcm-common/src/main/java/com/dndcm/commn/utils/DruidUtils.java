package com.dndcm.commn.utils;


import com.alibaba.druid.filter.config.ConfigTools;

/**
 * 数据库密码加密解密工具
 * @author lyy
 * @data 2021-6-1 17:32:47
 */
public class DruidUtils {
    /**
     * 加密
     * @param cipherText
     * @return
     * @throws Exception
     */
    public static String encrypt(String cipherText) throws Exception {
        String encrypt = ConfigTools.encrypt(cipherText);
        return encrypt;
    }

    /**
     * 解密
     * @param cipherText
     * @return
     * @throws Exception
     */
    public static String decrypt(String cipherText) throws Exception {
        String decrypt = ConfigTools.decrypt(cipherText);
        return decrypt;
    }

    public static void main(String[] args) throws Exception {
        String password = "123456";
        String encrypt = encrypt(password);
        System.out.println("密文password："+encrypt);
        String decrypt = decrypt(encrypt);
        System.out.println("明文password："+decrypt);

    }
}
