package com.metadata.gateway.dto;

import lombok.Data;

/**
 * @author liyy
 * @description
 * @date 2021-6-28 09:55:55
 **/
@Data
public class UserDto {

    private Long id;

    /**
     * 头像
     */
    private String avatar;

    private Long orgCode;

    /**
     * 账号
     */
    private String account;

    /**
     * 名字
     */
    private String name;

    /**
     * 电子邮件
     */
    private String email;

    /**
     * 电话
     */
    private String phone;

    /**
     * 1-启用，0-禁用
     */
    private Integer status;

}
