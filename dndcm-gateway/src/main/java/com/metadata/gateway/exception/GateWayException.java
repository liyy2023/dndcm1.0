package com.metadata.gateway.exception;

import com.metadata.gateway.constant.ResponseCode;

import java.util.Objects;

/**
 * 自定义网关异常
 * @author lyy
 * @date 2021-6-28 10:44:39
 */
public class GateWayException extends RuntimeException{

    private ResponseCode responseCodeEnum;
    private Object[] args;

    public GateWayException(ResponseCode responseCodeEnum, Object... codeMessageArgs) {
        super(detailMessage(responseCodeEnum, codeMessageArgs));
        this.responseCodeEnum = responseCodeEnum;
        this.args = codeMessageArgs;
    }

    public GateWayException(ResponseCode responseCodeEnum) {
        super(responseCodeEnum.getMessage());
        this.responseCodeEnum = responseCodeEnum;
    }

    public static String detailMessage(ResponseCode responseCodeEnum, Object[] args) {
        if (Objects.isNull(responseCodeEnum)) {
            return "[warn] code is null";
        } else {
            if (responseCodeEnum.getArgs() > 0) {
                if (Objects.isNull(args)) {
                    return "[warn] args is null";
                }

                if (responseCodeEnum.getArgs() != args.length) {
                    return String.format("[warn] args length mismatch: expect %d, actual %d", responseCodeEnum.getArgs(), args.length);
                }
            }

            return String.format(responseCodeEnum.getMessage(), args);
        }
    }


    public void setResponseCodeEnum(ResponseCode responseCodeEnum) {
        this.responseCodeEnum = responseCodeEnum;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public ResponseCode getResponseCodeEnum() {
        return this.responseCodeEnum;
    }

    public Object[] getArgs() {
        return this.args;
    }
}
