package com.metadata.gateway.constant;

/**
 * 响应码
 * @author lyy
 */
public interface ResponseCode {
    /**
     * 响应名
     * @return
     */
    String name();
    /**
     * 响应码
     * @return
     */
    String getCode();
    /**
     * 响应信息
     * @return
     */
    String getMessage();
    /**
     * 响应参数
     * @return
     */
    int getArgs();

    /**
     * 描述
     * @return
     */
    default String description() {
        return this.name() + '-' + this.getCode() + '-' + this.getMessage() + '-' + this.getArgs();
    }
}
