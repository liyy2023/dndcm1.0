package com.metadata.gateway.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang3.time.DateUtils;

public class TokenUtil {
    public TokenUtil() {
    }

    public static String creatToken(String secret, Integer expireDate, Map<String, String> claim) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            String token = "";
            Builder builder = JWT.create();
            builder.withIssuer("auth0");
            builder.withIssuedAt(new Date());
            builder.withExpiresAt(DateUtils.addSeconds(new Date(), expireDate));
            claim.forEach(builder::withClaim);
            token = builder.sign(algorithm);
            return token;
        } catch (JWTCreationException var6) {
            var6.printStackTrace();
            return null;
        }
    }

    public static Boolean verifierToken(String secret, String token, Map<String, String> claim) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            Verification require = JWT.require(algorithm);
            require.withIssuer(new String[]{"auth0"});
            claim.forEach(require::withClaim);
            JWTVerifier verifier = require.build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getExpiresAt().compareTo(new Date()) >= 0;
        } catch (JWTVerificationException var7) {
            return false;
        }
    }

    private static DecodedJWT decodedJWT(String token) {
        return JWT.decode(token);
    }

    public static String getAlgorithm(String token) {
        return decodedJWT(token).getAlgorithm();
    }

    public static Date getExpiresAt(String token) {
        return decodedJWT(token).getExpiresAt();
    }

    public static Date getIssuedAt(String token) {
        return decodedJWT(token).getIssuedAt();
    }

    public static String getIssuer(String token) {
        return decodedJWT(token).getIssuer();
    }

    public static Map<String, Claim> getClaims(String token) {
        Map<String, Claim> claims = decodedJWT(token).getClaims();
        return claims;
    }

    public static String getClaimByKey(String token, String key) {
        Claim claim = decodedJWT(token).getClaim(key);
        return claim.asString();
    }
}
