package com.metadata.gateway.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author wuhuaming
 * @description
 * @date 2021-02-24 12:21
 **/
@Component
public class TokenService {
    @Value("${token.private.secret}")
    private  String secret;
    @Value("${token.expiration}")
    private Integer expiration;

    public String createToken(Map<String, String> claim){
        return TokenUtil.creatToken(secret,expiration,claim);
    }

    public String getUsername(String token, String key){
        return TokenUtil.getClaimByKey(token,key);
    }

    public Boolean verifierToken(String token,Map<String, String> claim){
        return TokenUtil.verifierToken(secret,token,claim);
    }
}
