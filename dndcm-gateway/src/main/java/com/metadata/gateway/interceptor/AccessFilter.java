package com.metadata.gateway.interceptor;


import com.metadata.gateway.enums.CommonResponseEnum;
import com.metadata.gateway.enums.ServiceExceptionEnum;
import com.metadata.gateway.exception.GateWayException;
import com.metadata.gateway.properties.IgnoreWhiteProperties;
import com.metadata.gateway.util.RedisUtil;
import com.metadata.gateway.util.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liyy
 * @description 统一认证：
 * @date 2021-6-28 10:54:15
 **/
@Slf4j
@Component
public class AccessFilter implements GlobalFilter, Ordered {

    @Autowired
    private IgnoreWhiteProperties ignoreWhite;

    @Autowired
    private RedisUtil redisUtil;

    @Value("${gateway.token.enable:false}")
    private Boolean enableToken;

    @Value("${token.tokenHeader:token}")
    private String tokenHeader;

    @Autowired
    private TokenService tokenService;


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("进入网关");
//        if(enableToken){
//            String url = exchange.getRequest().getURI().getPath();
//            List<String> list = ignoreWhite.getWhites();
//            // 跳过不需要验证的路径
//            for(String str : list){
//                if(url.equals(str)){
//                    return chain.filter(exchange);
//                }
//            }
//            // 获取请求头参数：
//            String token = exchange.getRequest().getQueryParams().getFirst(tokenHeader);
//            if(StringUtils.isEmpty(token)){
////            ServerHttpResponse response = exchange.getResponse();
//                // 响应用户 token 为空
//                throw new GateWayException(CommonResponseEnum.NotLogin);
//            }
//            // 解析token 获取用户唯一标识
//            String phone = tokenService.getUsername(token, "phone");
//            // 验证token是否匹配
//            String tokenPrefix = "admin:token:";
//            String redisToken = (String)redisUtil.getCacheObject(tokenPrefix + phone);
//            if(StringUtils.isEmpty(redisToken)){
//                throw new GateWayException(CommonResponseEnum.NotLogin);
//            }else if(!token.equals(redisToken)){
//                throw new GateWayException(ServiceExceptionEnum.TokenNotMatch);
//            }
//            // 验证 token 是否过期
//            Map<String,String> claim = new HashMap<>();
//            claim.put("phone",phone);
//            if(!tokenService.verifierToken(token,claim)){
//                throw new GateWayException(ServiceExceptionEnum.TokenIsOverdue);
//            }
//        }
        // 放行
        return chain.filter(exchange);
    }

    /**
     * 过滤器执行顺序，数值越小，优先级越高
     * @return
     */
    @Override
    public int getOrder() {
        return 1;
    }
}
