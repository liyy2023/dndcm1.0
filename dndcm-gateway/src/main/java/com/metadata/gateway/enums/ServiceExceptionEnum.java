package com.metadata.gateway.enums;


import com.metadata.gateway.constant.ResponseCode;

/**
 * @author wuhuaming
 * @description
 * @date 2021-02-24 21:46
 **/
public enum ServiceExceptionEnum implements ResponseCode {
    TokenIsOverdue("999901010","token已过期",0),
    TokenNotMatch("999901009","token 不匹配",0)
    ;

    ServiceExceptionEnum(String code, String message, int args) {
        this.code = code;
        this.message = message;
        this.args = args;
    }

    private String code;

    private String message;

    private int args;

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setArgs(int args) {
        this.args = args;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public int getArgs() {
        return 0;
    }
}
