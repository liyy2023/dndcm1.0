package com.metadata.gateway.enums;

import com.metadata.gateway.constant.ResponseCode;

/**
 * @author lyy
 * @date 2021-6-28 10:52:34
 */
public enum CommonResponseEnum implements ResponseCode {
    Success("99990000", "操作成功", 0),
    ClientError("99990006", "客户端异常", 0),
    ServerError("99990001", "服务器错误", 0),
    IllegalArgument("99990002", "参数错误：%s", 1),
    NoPermission("99990003", "无权限", 0),
    NeedRefreshToken("99990004", "需要刷新Token的TTL", 0),
    NotLogin("99990005", "用户未登录", 0),
    NotFound("99990011", "不存在", 0),
    AlreadyExists("99990012", "已存在", 0),
    AlreadyLocked("99990021", "已锁定", 0),
    LockFailure("99990022", "锁定异常", 0),
    DbFailure("99990031", "数据库异常", 0),
    NetworkFailure("99990032", "网络异常", 0),
    AdaptorFailure("99990033", "外部接口异常", 0),
    DbTransactionException("99990034", "数据库事务异常", 0),
    ServerShutDown("99990098", "服务器关闭", 0),
    NullPointerException("99990100", "空指针异常", 0),
    ServerBusy("99990099", "服务器忙", 0);

    private String code;
    private String message;
    private int args;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public int getArgs() {
        return this.args;
    }

    private CommonResponseEnum(String code, String message, int args) {
        this.code = code;
        this.message = message;
        this.args = args;
    }
}
