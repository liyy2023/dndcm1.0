package com.dndcm.sys.mapper;

import com.dndcm.sys.api.entity.SysJobs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 岗位信息  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysJobsMapper extends BaseMapper<SysJobs> {

}
