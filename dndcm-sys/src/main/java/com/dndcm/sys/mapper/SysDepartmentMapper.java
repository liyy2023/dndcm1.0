package com.dndcm.sys.mapper;

import com.dndcm.sys.api.entity.SysDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门信息表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysDepartmentMapper extends BaseMapper<SysDepartment> {

}
