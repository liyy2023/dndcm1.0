package com.dndcm.sys.mapper;

import com.dndcm.sys.api.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.sys.api.entity.vo.SysUserVo;
import com.dndcm.sys.input.SysUserPageInput;

import java.util.List;

/**
 * <p>
 * 用户信息  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 详情
     * @param sysUser
     * @return
     */
    SysUserVo queryDetail(SysUser sysUser);


    /**
     * 列表
     * @param pageInput
     * @return
     */
    List<SysUserVo> queryList(SysUserPageInput pageInput);



}
