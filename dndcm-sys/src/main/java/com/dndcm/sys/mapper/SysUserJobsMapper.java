package com.dndcm.sys.mapper;

import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.SysUserJobs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户关联岗位信息表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysUserJobsMapper extends BaseMapper<SysUserJobs> {

    /**
     * @param sysUser
     * @return
     */
    List<String> queryJobs(SysUser sysUser);

}
