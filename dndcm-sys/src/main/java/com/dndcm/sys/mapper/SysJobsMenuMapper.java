package com.dndcm.sys.mapper;

import com.dndcm.sys.api.entity.SysJobsMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 岗位菜单关系表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysJobsMenuMapper extends BaseMapper<SysJobsMenu> {

}
