package com.dndcm.sys.service.impl;

import com.dndcm.sys.api.entity.SysJobsMenu;
import com.dndcm.sys.mapper.SysJobsMenuMapper;
import com.dndcm.sys.service.SysJobsMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位菜单关系表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
@Service
public class SysJobsMenuServiceImpl extends ServiceImpl<SysJobsMenuMapper, SysJobsMenu> implements SysJobsMenuService {

}
