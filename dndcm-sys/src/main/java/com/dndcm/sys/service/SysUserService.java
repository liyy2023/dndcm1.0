package com.dndcm.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dndcm.sys.api.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.sys.api.entity.vo.SysUserVo;
import com.dndcm.sys.input.SysUserPageInput;

import java.util.List;

/**
 * <p>
 * 用户信息  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 删除
     * @param sysUser
     * @return
     */
    Boolean delete(SysUser sysUser);

    /**
     * 查询详情
     * @param sysUser
     * @return
     */
    SysUserVo queryDetail(SysUser sysUser);

    /**
     * 查询列表
     * @param pageInput
     * @return
     */
    List<SysUserVo> queryList(SysUserPageInput pageInput);

    /**
     * 分页查询
     * @param pageInput
     * @return
     */
    List<SysUserVo> queryPage(SysUserPageInput pageInput);

}
