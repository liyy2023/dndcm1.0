package com.dndcm.sys.service.impl;

import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.SysUserJobs;
import com.dndcm.sys.mapper.SysUserJobsMapper;
import com.dndcm.sys.service.SysUserJobsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户关联岗位信息表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
@Service
public class SysUserJobsServiceImpl extends ServiceImpl<SysUserJobsMapper, SysUserJobs> implements SysUserJobsService {

    @Autowired
    SysUserJobsMapper sysUserJobsMapper;

    @Override
    public List<String> queryJobs(SysUser sysUser) {
        return null;
    }
}
