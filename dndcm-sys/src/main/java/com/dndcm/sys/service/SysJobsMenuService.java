package com.dndcm.sys.service;

import com.dndcm.sys.api.entity.SysJobsMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 岗位菜单关系表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysJobsMenuService extends IService<SysJobsMenu> {

}
