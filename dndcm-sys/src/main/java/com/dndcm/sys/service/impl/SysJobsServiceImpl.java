package com.dndcm.sys.service.impl;

import com.dndcm.sys.api.entity.SysJobs;
import com.dndcm.sys.mapper.SysJobsMapper;
import com.dndcm.sys.service.SysJobsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位信息  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
@Service
public class SysJobsServiceImpl extends ServiceImpl<SysJobsMapper, SysJobs> implements SysJobsService {

}
