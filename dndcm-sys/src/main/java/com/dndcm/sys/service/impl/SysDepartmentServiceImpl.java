package com.dndcm.sys.service.impl;

import com.dndcm.sys.api.entity.SysDepartment;
import com.dndcm.sys.mapper.SysDepartmentMapper;
import com.dndcm.sys.service.SysDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门信息表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
@Service
public class SysDepartmentServiceImpl extends ServiceImpl<SysDepartmentMapper, SysDepartment> implements SysDepartmentService {

}
