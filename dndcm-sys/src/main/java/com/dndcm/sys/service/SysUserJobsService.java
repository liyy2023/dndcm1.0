package com.dndcm.sys.service;

import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.SysUserJobs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户关联岗位信息表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysUserJobsService extends IService<SysUserJobs> {

    /**
     * 查询岗位
     * @param sysUser
     * @return
     */
    List<String> queryJobs(SysUser sysUser);

}
