package com.dndcm.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dndcm.commn.constant.enums.DeleteYnEnum;
import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.vo.SysUserVo;
import com.dndcm.sys.input.SysUserPageInput;
import com.dndcm.sys.mapper.SysUserMapper;
import com.dndcm.sys.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户信息  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    SysUserMapper sysUserMapper;

    @Override
    public Boolean delete(SysUser sysUser) {
        sysUser.setDeleteYn(DeleteYnEnum.YES.getCode());
        return this.updateById(sysUser);
    }

    @Override
    public SysUserVo queryDetail(SysUser sysUser) {
        return sysUserMapper.queryDetail(sysUser);
    }

    @Override
    public List<SysUserVo> queryList(SysUserPageInput pageInput) {
        return sysUserMapper.queryList(pageInput);
    }

    @Override
    public List<SysUserVo> queryPage(SysUserPageInput pageInput) {
        PageHelper.startPage(pageInput.getPageType().getPageNo(),pageInput.getPageType().getPageSize());
        return sysUserMapper.queryList(pageInput);
    }
}
