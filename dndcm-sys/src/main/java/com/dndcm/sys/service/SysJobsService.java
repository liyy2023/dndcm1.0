package com.dndcm.sys.service;

import com.dndcm.sys.api.entity.SysJobs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 岗位信息  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-08
 */
public interface SysJobsService extends IService<SysJobs> {

}
