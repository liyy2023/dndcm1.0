package com.dndcm.sys.input;

import com.dndcm.commn.request.AbstractPageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系统用户分页入参
 * @author lyy
 * @date 2021-7-13 14:31:42
 */
@Data
public class SysUserPageInput extends AbstractPageRequest {

    @ApiModelProperty(value = "用户账号")
    private String userAccount;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "用户等级")
    private Integer userLevel;

    @ApiModelProperty(value = "用户部门")
    private Long userDepartmentId;

    @ApiModelProperty(value = "用户状态")
    private Integer userStatus;
}
