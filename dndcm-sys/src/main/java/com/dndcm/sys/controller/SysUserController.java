package com.dndcm.sys.controller;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.vo.SysUserVo;
import com.dndcm.sys.input.SysUserPageInput;
import com.dndcm.sys.service.SysUserJobsService;
import com.dndcm.sys.service.SysUserService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统用户控制器
 * @author lyy
 * @date
 */
@Slf4j
@Api(tags = "系统用户控制器")
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    SysUserService userService;
    @Autowired
    SysUserJobsService userJobsService;

    @RequestMapping(method = {RequestMethod.POST}, value = "/save")
    @ResponseBody
    @ApiOperation(value = "新增",notes = "新增")
    public CommonResponse<Boolean> create(SysUser sysUser){
        return CommonResponse.ok(userService.save(sysUser));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/edit")
    @ResponseBody
    @ApiOperation(value = "编辑",notes = "编辑")
    public CommonResponse<Boolean> edit(SysUser sysUser){
        return CommonResponse.ok(userService.updateById(sysUser));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/delete")
    @ResponseBody
    @ApiOperation(value = "删除",notes = "删除")
    public CommonResponse<Boolean> delete(SysUser sysUser){
        return CommonResponse.ok(userService.delete(sysUser));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/query")
    @ResponseBody
    @ApiOperation(value = "查询详情",notes = "查询详情")
    public CommonResponse<SysUserVo> queryDetail(SysUser sysUser){
        return CommonResponse.ok(userService.queryDetail(sysUser));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/queryPage")
    @ResponseBody
    @ApiOperation(value = "查询列表",notes = "查询列表")
    public CommonResponse<PageInfo<SysUserVo>> queryPage(SysUserPageInput pageInput){
        return CommonResponse.ok(new PageInfo<>(userService.queryPage(pageInput)));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/queryJobs")
    @ResponseBody
    @ApiOperation(value = "查询岗位",notes = "查询岗位")
    public CommonResponse<List<String>> queryJobs(SysUser sysUser){
        return CommonResponse.ok(userJobsService.queryJobs(sysUser));
    }


}
