package com.dndcm.stream.controller;

import com.dndcm.commn.constant.enums.CommonResponseEnum;
import com.dndcm.commn.response.CommonResponse;
import com.dndcm.stream.api.entity.TaskProcedure;
import com.dndcm.stream.api.entity.vo.ProcedureTreeVo;
import com.dndcm.stream.api.entity.vo.ProcedureVo;
import com.dndcm.stream.service.TaskProcedureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 任务流程控制器
 * @author lyy
 * @date
 */
@Slf4j
@Api(tags = "任务流程控制器")
@RestController
@RequestMapping("/procedure")
public class TaskProcedureController {

    @Autowired
    private TaskProcedureService taskProcedureService;

    @RequestMapping(method = {RequestMethod.POST}, value = "/creat")
    @ResponseBody
    @ApiOperation(value = "新增流程",notes = "新增流程")
    public CommonResponse<Boolean> createProcedure(@RequestBody TaskProcedure taskProcedure) throws Exception{
        return CommonResponse.ok(taskProcedureService.save(taskProcedure));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/edit")
    @ResponseBody
    @ApiOperation(value = "编辑流程",notes = "编辑流程")
    public CommonResponse<Boolean> editProcedure(@RequestBody TaskProcedure taskProcedure) throws Exception{
        return CommonResponse.ok(taskProcedureService.updateById(taskProcedure));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/delete")
    @ResponseBody
    @ApiOperation(value = "删除流程",notes = "删除流程")
    public CommonResponse<Boolean> deleteProcedure(@RequestBody TaskProcedure taskProcedure) throws Exception{
        return CommonResponse.ok(taskProcedureService.delete(taskProcedure));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/queryTree")
    @ResponseBody
    @ApiOperation(value = "查询流程列表",notes = "查询流程列表")
    public CommonResponse<ProcedureTreeVo> queryTree(@RequestBody TaskProcedure taskProcedure) throws Exception{
        return CommonResponse.ok(taskProcedureService.queryTree());
    }


    @RequestMapping(method = {RequestMethod.POST}, value = "/query")
    @ResponseBody
    @ApiOperation(value = "流程详情",notes = "流程详情")
    public CommonResponse<ProcedureVo> queryProcedure(@RequestBody TaskProcedure taskProcedure) throws Exception{
        return CommonResponse.ok(taskProcedureService.queryProcedure(taskProcedure));
    }

}
