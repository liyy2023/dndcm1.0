package com.dndcm.stream.controller;

import com.dndcm.stream.api.entity.ProjectCategory;
import com.dndcm.stream.service.ProjectCategoryService;
import com.dndcm.project.api.Work;
import com.dndcm.project.api.feign.WorkFeign;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 测试控制器
 * @author lyy
 * @date 2021-7-7 18:59:57
 */
@Slf4j
@Api(tags = "测试控制器")
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private ProjectCategoryService projectCategoryService;
    @Autowired
    private WorkFeign workFeign;

    @RequestMapping(method = {RequestMethod.POST}, value = "/queryCategoryInfo")
    @ResponseBody
    @ApiOperation(value = "查看api详情",notes = "查看api详情")
    public ProjectCategory queryCategoryInfo(@RequestBody ProjectCategory record) throws Exception {
        Work w = new Work();
        w.setName("test");
        workFeign.getWork(w);
        log.info(w.getName());
        return projectCategoryService.selectById(record);
    }
}
