package com.dndcm.stream.controller;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.stream.api.entity.TaskNodeBindingDocument;
import com.dndcm.stream.api.entity.TaskNodeIntegrityCheckConfig;
import com.dndcm.stream.service.TaskNodeBindingDocumentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 节点绑定文档控制器
 * @author lyy
 * @date 2021-7-13 11:53:40
 */
@Slf4j
@Api(tags = "节点绑定文档控制器")
@RestController
@RequestMapping("/nodeBindingDoc")
public class NodeBindingDocumentController {

    @Autowired
    TaskNodeBindingDocumentService bindingDocumentService;

    @RequestMapping(method = {RequestMethod.POST}, value = "/save")
    @ResponseBody
    @ApiOperation(value = "新增",notes = "新增")
    public CommonResponse<Boolean> create(TaskNodeBindingDocument bindingDocument){
        return CommonResponse.ok(bindingDocumentService.save(bindingDocument));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/edit")
    @ResponseBody
    @ApiOperation(value = "编辑",notes = "编辑")
    public CommonResponse<Boolean> edit(TaskNodeBindingDocument bindingDocument){
        return CommonResponse.ok(bindingDocumentService.updateById(bindingDocument));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/delete")
    @ResponseBody
    @ApiOperation(value = "删除",notes = "删除")
    public CommonResponse<Boolean> delete(TaskNodeBindingDocument bindingDocument){
        return CommonResponse.ok(bindingDocumentService.delete(bindingDocument));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/query")
    @ResponseBody
    @ApiOperation(value = "查询详情",notes = "查询详情")
    public CommonResponse<TaskNodeBindingDocument> query(TaskNodeBindingDocument bindingDocument){
        return CommonResponse.ok(bindingDocumentService.queryDetail(bindingDocument));
    }
    @RequestMapping(method = {RequestMethod.POST}, value = "/queryList")
    @ResponseBody
    @ApiOperation(value = "查询列表",notes = "查询列表")
    public CommonResponse<List<TaskNodeBindingDocument>> queryList(TaskNodeBindingDocument bindingDocument){
        return CommonResponse.ok(bindingDocumentService.queryList(bindingDocument));
    }
}
