package com.dndcm.stream.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dndcm.commn.response.CommonResponse;
import com.dndcm.stream.api.entity.TaskNodeMainConfig;
import com.dndcm.stream.input.NodePageInput;
import com.dndcm.stream.service.TaskNodeMainConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流程节点控制器
 * @author lyy
 * @date 2021-7-12 14:45:23
 */
@Slf4j
@Api(tags = "流程节点控制器")
@RestController
@RequestMapping("/node")
public class TaskNodeController {

    @Autowired
    TaskNodeMainConfigService taskNodeMainConfigService;

    @RequestMapping(method = {RequestMethod.POST}, value = "/creat")
    @ResponseBody
    @ApiOperation(value = "新增节点",notes = "新增节点")
    public CommonResponse<Boolean> createNode(TaskNodeMainConfig node){
        return CommonResponse.ok(taskNodeMainConfigService.save(node));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/edit")
    @ResponseBody
    @ApiOperation(value = "修改节点",notes = "修改节点")
    public CommonResponse<Boolean> editNode(TaskNodeMainConfig node){
        return CommonResponse.ok(taskNodeMainConfigService.updateById(node));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/delete")
    @ResponseBody
    @ApiOperation(value = "删除节点",notes = "删除节点")
    public CommonResponse<Boolean> deleteNode(TaskNodeMainConfig node){
        return CommonResponse.ok(taskNodeMainConfigService.delete(node));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/queryPage")
    @ResponseBody
    @ApiOperation(value = "查询节点列表",notes = "查询节点列表")
    public IPage<TaskNodeMainConfig> queryNodeList(NodePageInput input){
        return taskNodeMainConfigService.queryNodeList(input);
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/query")
    @ResponseBody
    @ApiOperation(value = "查询节点",notes = "查询节点")
    public CommonResponse<TaskNodeMainConfig> queryNode(TaskNodeMainConfig node){
        return CommonResponse.ok(taskNodeMainConfigService.queryNode(node));
    }


}
