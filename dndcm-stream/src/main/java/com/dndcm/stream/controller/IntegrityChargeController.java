package com.dndcm.stream.controller;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.stream.api.entity.IntegrityFunction;
import com.dndcm.stream.api.entity.TaskNodeIntegrityCheckConfig;
import com.dndcm.stream.service.IntegrityFunctionConfigService;
import com.dndcm.stream.service.TaskNodeIntegrityCheckConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 完整性检查控制器
 * @author lyy
 * @date 2021-7-13 10:34:28
 */
@Slf4j
@Api(tags = "完整性检查控制器")
@RestController
@RequestMapping("/integrity")
public class IntegrityChargeController {

    @Autowired
    TaskNodeIntegrityCheckConfigService checkConfigService;

    @Autowired
    IntegrityFunctionConfigService functionConfigService;

    @RequestMapping(method = {RequestMethod.POST}, value = "/query")
    @ResponseBody
    @ApiOperation(value = "查询列表",notes = "查询列表")
    public CommonResponse<List<TaskNodeIntegrityCheckConfig>> queryList(TaskNodeIntegrityCheckConfig config){
        return CommonResponse.ok(checkConfigService.queryList(config));
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/queryTools")
    @ResponseBody
    @ApiOperation(value = "查询工具栏",notes = "查询工具栏")
    public CommonResponse<List<IntegrityFunction>> queryFunction(TaskNodeIntegrityCheckConfig config){
        return CommonResponse.ok(null);
    }


}
