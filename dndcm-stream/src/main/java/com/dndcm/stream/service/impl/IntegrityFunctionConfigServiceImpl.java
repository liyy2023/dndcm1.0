package com.dndcm.stream.service.impl;

import com.dndcm.stream.api.entity.IntegrityFunction;
import com.dndcm.stream.api.entity.IntegrityFunctionConfig;
import com.dndcm.stream.mapper.IntegrityFunctionConfigMapper;
import com.dndcm.stream.service.IntegrityFunctionConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 完整性功能配置表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
@Service
public class IntegrityFunctionConfigServiceImpl extends ServiceImpl<IntegrityFunctionConfigMapper, IntegrityFunctionConfig> implements IntegrityFunctionConfigService {

    @Autowired
    IntegrityFunctionConfigMapper functionConfigMapper;

    @Override
    public List<IntegrityFunction> queryFunction(IntegrityFunctionConfig config) {
        return functionConfigMapper.queryFunction(config);
    }
}
