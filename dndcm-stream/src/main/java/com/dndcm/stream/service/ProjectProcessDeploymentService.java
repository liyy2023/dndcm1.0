package com.dndcm.stream.service;

import com.dndcm.stream.api.entity.ProjectProcessDeployment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目流程部署表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface ProjectProcessDeploymentService extends IService<ProjectProcessDeployment> {

}
