package com.dndcm.stream.service.impl;

import com.dndcm.stream.api.entity.ProjectCategory;
import com.dndcm.stream.mapper.ProjectCategoryMapper;
import com.dndcm.stream.service.ProjectCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目类别信息表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-07
 */
@Service
public class ProjectCategoryServiceImpl extends ServiceImpl<ProjectCategoryMapper, ProjectCategory> implements ProjectCategoryService {

    @Override
    public ProjectCategory selectById(ProjectCategory record) {
        return this.baseMapper.selectById(record.getId());
    }
}
