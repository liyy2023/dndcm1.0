package com.dndcm.stream.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dndcm.commn.constant.enums.DeleteYnEnum;
import com.dndcm.stream.api.entity.TaskNodeIntegrityCheckConfig;
import com.dndcm.stream.mapper.TaskNodeIntegrityCheckConfigMapper;
import com.dndcm.stream.service.TaskNodeIntegrityCheckConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 节点完整性检查配置表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
@Service
public class TaskNodeIntegrityCheckConfigServiceImpl extends ServiceImpl<TaskNodeIntegrityCheckConfigMapper, TaskNodeIntegrityCheckConfig> implements TaskNodeIntegrityCheckConfigService {

    @Override
    public List<TaskNodeIntegrityCheckConfig> queryList(TaskNodeIntegrityCheckConfig config) {
        LambdaQueryWrapper<TaskNodeIntegrityCheckConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(config.getDepartmentId() != null,TaskNodeIntegrityCheckConfig::getDepartmentId,config.getDepartmentId());
        wrapper.eq(TaskNodeIntegrityCheckConfig::getDeleteYn, DeleteYnEnum.NO.getCode());
        return this.baseMapper.selectList(wrapper);
    }
}
