package com.dndcm.stream.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dndcm.commn.constant.enums.DeleteYnEnum;
import com.dndcm.stream.api.entity.TaskNodeMainConfig;
import com.dndcm.stream.api.entity.TaskProcedure;
import com.dndcm.stream.api.entity.vo.ProcedureTreeVo;
import com.dndcm.stream.api.entity.vo.ProcedureVo;
import com.dndcm.stream.mapper.TaskNodeMainConfigMapper;
import com.dndcm.stream.mapper.TaskProcedureMapper;
import com.dndcm.stream.service.TaskProcedureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 任务流程表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class TaskProcedureServiceImpl extends ServiceImpl<TaskProcedureMapper, TaskProcedure> implements TaskProcedureService {

    @Autowired
    TaskProcedureMapper taskProcedureMapper;

    @Autowired
    TaskNodeMainConfigMapper taskNodeMainConfigMapper;

    @Override
    public Boolean edit(TaskProcedure taskProcedure) {
        return null;
    }

    @Override
    public Boolean delete(TaskProcedure taskProcedure) {
        taskProcedure.setDeleteYn(DeleteYnEnum.YES.getCode());
        return this.updateById(taskProcedure);
    }

    @Override
    public ProcedureTreeVo queryTree() {
        return null;
    }

    @Override
    public ProcedureVo queryProcedure(TaskProcedure taskProcedure) {
        Long procedureId = taskProcedure.getId();
        TaskProcedure procedure = this.getById(procedureId);
        ProcedureVo procedureVo = new ProcedureVo();
        BeanUtils.copyProperties(procedure,procedureVo);
        LambdaQueryWrapper<TaskNodeMainConfig> nodeWrapper = new LambdaQueryWrapper<>();
        nodeWrapper.eq(procedureId != null,TaskNodeMainConfig::getDeployId,procedureId);
        List<TaskNodeMainConfig> nodeList = taskNodeMainConfigMapper.selectList(nodeWrapper);
        procedureVo.setNodeList(nodeList);
        return procedureVo;
    }
}
