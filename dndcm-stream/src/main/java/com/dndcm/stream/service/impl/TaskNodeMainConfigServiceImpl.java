package com.dndcm.stream.service.impl;



import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dndcm.commn.constant.enums.DeleteYnEnum;
import com.dndcm.stream.api.entity.TaskNodeMainConfig;
import com.dndcm.stream.input.NodePageInput;
import com.dndcm.stream.mapper.TaskNodeMainConfigMapper;
import com.dndcm.stream.service.TaskNodeMainConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务节点配置表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class TaskNodeMainConfigServiceImpl extends ServiceImpl<TaskNodeMainConfigMapper, TaskNodeMainConfig> implements TaskNodeMainConfigService {


    @Override
    public Boolean delete(TaskNodeMainConfig taskNodeMainConfig) {
        taskNodeMainConfig.setDeleteYn(DeleteYnEnum.YES.getCode());
        return this.updateById(taskNodeMainConfig);
    }

    @Override
    public IPage<TaskNodeMainConfig> queryNodeList(NodePageInput input) {
        IPage iPage = new Page();
        iPage.setSize(input.getPageType().getPageSize());
        iPage.setCurrent(input.getPageType().getPageNo());
        LambdaQueryWrapper<TaskNodeMainConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(input.getDeployId() != null,TaskNodeMainConfig::getDeployId,input.getDeployId());
        return this.baseMapper.selectPage(iPage,wrapper);
    }

    @Override
    public TaskNodeMainConfig queryNode(TaskNodeMainConfig taskNodeMainConfig) {
        return this.getById(taskNodeMainConfig.getId());
    }
}
