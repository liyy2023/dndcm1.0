package com.dndcm.stream.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.stream.api.entity.TaskNodeMainConfig;
import com.dndcm.stream.input.NodePageInput;

/**
 * <p>
 * 任务节点配置表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskNodeMainConfigService extends IService<TaskNodeMainConfig> {

    /**
     * @param taskNodeMainConfig
     * @return
     */
    Boolean delete (TaskNodeMainConfig taskNodeMainConfig);

    /**
     * @param input
     * @return
     */
    IPage<TaskNodeMainConfig> queryNodeList(NodePageInput input);

    /**
     * @param taskNodeMainConfig
     * @return
     */
    TaskNodeMainConfig queryNode(TaskNodeMainConfig taskNodeMainConfig);

}
