package com.dndcm.stream.service;

import com.dndcm.stream.api.entity.ProjectCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目类别信息表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-07
 */
public interface ProjectCategoryService extends IService<ProjectCategory> {

    ProjectCategory selectById(ProjectCategory record);
}
