package com.dndcm.stream.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.stream.api.entity.TaskNodePermission;

/**
 * <p>
 * 任务节点权限表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskNodePermissionService extends IService<TaskNodePermission> {

}
