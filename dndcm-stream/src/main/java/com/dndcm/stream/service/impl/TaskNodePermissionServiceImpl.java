package com.dndcm.stream.service.impl;


import com.dndcm.stream.api.entity.TaskNodePermission;
import com.dndcm.stream.mapper.TaskNodePermissionMapper;
import com.dndcm.stream.service.TaskNodePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务节点权限表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class TaskNodePermissionServiceImpl extends ServiceImpl<TaskNodePermissionMapper, TaskNodePermission> implements TaskNodePermissionService {

}
