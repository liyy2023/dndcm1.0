package com.dndcm.stream.service;

import com.dndcm.stream.api.entity.TaskNodeIntegrityCheckConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 节点完整性检查配置表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface TaskNodeIntegrityCheckConfigService extends IService<TaskNodeIntegrityCheckConfig> {

    /**
     * 查询列表
     * @param config
     * @return
     */
    List<TaskNodeIntegrityCheckConfig> queryList(TaskNodeIntegrityCheckConfig config);

}
