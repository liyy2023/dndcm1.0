package com.dndcm.stream.service.impl;

import com.dndcm.stream.api.entity.IntegrityFunction;
import com.dndcm.stream.mapper.IntegrityFunctionMapper;
import com.dndcm.stream.service.IntegrityFunctionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 完整性功能表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
@Service
public class IntegrityFunctionServiceImpl extends ServiceImpl<IntegrityFunctionMapper, IntegrityFunction> implements IntegrityFunctionService {

}
