package com.dndcm.stream.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.stream.api.entity.TaskNodeBindingDocument;

import java.util.List;

/**
 * <p>
 * 节点绑定文档信息表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskNodeBindingDocumentService extends IService<TaskNodeBindingDocument> {

    Boolean delete(TaskNodeBindingDocument bindingDocument);

    TaskNodeBindingDocument queryDetail(TaskNodeBindingDocument bindingDocument);

    List<TaskNodeBindingDocument> queryList(TaskNodeBindingDocument bindingDocument);

}
