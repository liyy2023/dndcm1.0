package com.dndcm.stream.service;

import com.dndcm.stream.api.entity.IntegrityFunction;
import com.dndcm.stream.api.entity.IntegrityFunctionConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 完整性功能配置表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface IntegrityFunctionConfigService extends IService<IntegrityFunctionConfig> {

    /**
     * 查询功能工具栏
     * @param config
     * @return
     */
    List<IntegrityFunction> queryFunction(IntegrityFunctionConfig config);

}
