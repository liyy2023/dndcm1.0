package com.dndcm.stream.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.stream.api.entity.TaskProcedure;
import com.dndcm.stream.api.entity.vo.ProcedureTreeVo;
import com.dndcm.stream.api.entity.vo.ProcedureVo;

/**
 * <p>
 * 任务流程表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskProcedureService extends IService<TaskProcedure> {


    /**
     * 编辑
     * @param taskProcedure
     * @return
     */
    Boolean edit(TaskProcedure taskProcedure);
    /**
     * 删除
     * @param taskProcedure
     * @return
     */
    Boolean delete(TaskProcedure taskProcedure);

    /**
     * 流程树
     * @return
     */
    ProcedureTreeVo queryTree();

    /**
     * 流程详情
     * @param taskProcedure
     * @return
     */
    ProcedureVo queryProcedure(TaskProcedure taskProcedure);

}
