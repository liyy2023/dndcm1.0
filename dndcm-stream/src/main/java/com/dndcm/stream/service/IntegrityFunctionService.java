package com.dndcm.stream.service;

import com.dndcm.stream.api.entity.IntegrityFunction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 完整性功能表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface IntegrityFunctionService extends IService<IntegrityFunction> {

}
