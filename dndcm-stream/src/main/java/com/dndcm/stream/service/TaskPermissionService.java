package com.dndcm.stream.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.stream.api.entity.TaskPermission;

/**
 * <p>
 * 任务权限列表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskPermissionService extends IService<TaskPermission> {

}
