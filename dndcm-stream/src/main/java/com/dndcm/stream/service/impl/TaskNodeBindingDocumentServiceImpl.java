package com.dndcm.stream.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dndcm.commn.constant.enums.DeleteYnEnum;
import com.dndcm.stream.api.entity.TaskNodeBindingDocument;
import com.dndcm.stream.mapper.TaskNodeBindingDocumentMapper;
import com.dndcm.stream.service.TaskNodeBindingDocumentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 节点绑定文档信息表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class TaskNodeBindingDocumentServiceImpl extends ServiceImpl<TaskNodeBindingDocumentMapper, TaskNodeBindingDocument> implements TaskNodeBindingDocumentService {

    @Override
    public Boolean delete(TaskNodeBindingDocument bindingDocument) {
        bindingDocument.setDeleteYn(DeleteYnEnum.YES.getCode());
        return this.updateById(bindingDocument);
    }

    @Override
    public TaskNodeBindingDocument queryDetail(TaskNodeBindingDocument bindingDocument) {
        return this.baseMapper.selectById(bindingDocument.getId());
    }

    @Override
    public List<TaskNodeBindingDocument> queryList(TaskNodeBindingDocument bindingDocument) {
        LambdaQueryWrapper<TaskNodeBindingDocument> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(bindingDocument.getTaskNodeMainConfigId() != null,TaskNodeBindingDocument::getTaskNodeMainConfigId,bindingDocument.getTaskNodeMainConfigId());
        return this.baseMapper.selectList(wrapper);
    }
}
