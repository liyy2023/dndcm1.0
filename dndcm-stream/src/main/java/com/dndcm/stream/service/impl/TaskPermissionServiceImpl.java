package com.dndcm.stream.service.impl;


import com.dndcm.stream.api.entity.TaskPermission;
import com.dndcm.stream.mapper.TaskPermissionMapper;
import com.dndcm.stream.service.TaskPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务权限列表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class TaskPermissionServiceImpl extends ServiceImpl<TaskPermissionMapper, TaskPermission> implements TaskPermissionService {

}
