package com.dndcm.stream.service.impl;

import com.dndcm.stream.api.entity.ProjectProcessDeployment;
import com.dndcm.stream.mapper.ProjectProcessDeploymentMapper;
import com.dndcm.stream.service.ProjectProcessDeploymentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目流程部署表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
@Service
public class ProjectProcessDeploymentServiceImpl extends ServiceImpl<ProjectProcessDeploymentMapper, ProjectProcessDeployment> implements ProjectProcessDeploymentService {

}
