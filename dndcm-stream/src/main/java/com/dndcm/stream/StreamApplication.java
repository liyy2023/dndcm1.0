package com.dndcm.stream;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author lyy
 * @description
 * @date 2021-6-22 16:35:32
 **/
@Slf4j
@SpringBootApplication(scanBasePackages = {
        "com.dndcm.stream",
        "com.dndcm.stream.service",
        "com.dndcm.commn.config"
})
@MapperScan(basePackages = "com.dndcm.stream.mapper")
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.dndcm.project.api.feign"})
public class StreamApplication {
    public static void main(String[] args) {
        SpringApplication.run(StreamApplication.class,args);
        log.info("流程管理微服务模块启动成功。。。");
        log.info("swagger地址:http://localhost:9092/dndcm-stream/swagger-ui.html#/");
        log.info("访问地址:http://localhost:9092/dndcm-stream");

    }

}
