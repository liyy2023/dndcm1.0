package com.dndcm.stream.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.stream.api.entity.TaskProcedure;
import com.dndcm.stream.api.entity.vo.ProcedureVo;

/**
 * <p>
 * 任务流程表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskProcedureMapper extends BaseMapper<TaskProcedure> {

    /**
     * 查询流程详情
     * @param taskProcedure
     * @return
     */
    ProcedureVo queryProcedure(TaskProcedure taskProcedure);

}
