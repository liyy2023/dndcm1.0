package com.dndcm.stream.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.stream.api.entity.TaskPermission;

/**
 * <p>
 * 任务权限列表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskPermissionMapper extends BaseMapper<TaskPermission> {

}
