package com.dndcm.stream.mapper;

import com.dndcm.stream.api.entity.ProjectCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目类别信息表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-07
 */
public interface ProjectCategoryMapper extends BaseMapper<ProjectCategory> {

}
