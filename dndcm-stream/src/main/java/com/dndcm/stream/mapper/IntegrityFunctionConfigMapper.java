package com.dndcm.stream.mapper;

import com.dndcm.stream.api.entity.IntegrityFunction;
import com.dndcm.stream.api.entity.IntegrityFunctionConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 完整性功能配置表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface IntegrityFunctionConfigMapper extends BaseMapper<IntegrityFunctionConfig> {

    /**
     * 查询功能
     * @param config
     * @return
     */
    List<IntegrityFunction> queryFunction(IntegrityFunctionConfig config);

}
