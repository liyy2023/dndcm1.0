package com.dndcm.stream.mapper;

import com.dndcm.stream.api.entity.TaskNodeIntegrityCheckConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 节点完整性检查配置表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface TaskNodeIntegrityCheckConfigMapper extends BaseMapper<TaskNodeIntegrityCheckConfig> {

}
