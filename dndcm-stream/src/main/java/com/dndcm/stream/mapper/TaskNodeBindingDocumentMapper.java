package com.dndcm.stream.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.stream.api.entity.TaskNodeBindingDocument;

/**
 * <p>
 * 节点绑定文档信息表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskNodeBindingDocumentMapper extends BaseMapper<TaskNodeBindingDocument> {

}
