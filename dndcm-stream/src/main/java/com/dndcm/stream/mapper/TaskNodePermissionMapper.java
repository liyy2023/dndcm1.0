package com.dndcm.stream.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.stream.api.entity.TaskNodePermission;

/**
 * <p>
 * 任务节点权限表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface TaskNodePermissionMapper extends BaseMapper<TaskNodePermission> {

}
