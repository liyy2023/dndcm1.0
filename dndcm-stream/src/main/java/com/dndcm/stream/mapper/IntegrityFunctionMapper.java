package com.dndcm.stream.mapper;

import com.dndcm.stream.api.entity.IntegrityFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 完整性功能表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface IntegrityFunctionMapper extends BaseMapper<IntegrityFunction> {

}
