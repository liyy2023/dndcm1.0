package com.dndcm.stream.mapper;

import com.dndcm.stream.api.entity.ProjectProcessDeployment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目流程部署表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-12
 */
public interface ProjectProcessDeploymentMapper extends BaseMapper<ProjectProcessDeployment> {

}
