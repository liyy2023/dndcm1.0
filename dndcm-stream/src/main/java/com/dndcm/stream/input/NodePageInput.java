package com.dndcm.stream.input;

import com.dndcm.commn.request.AbstractPageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 节点分页列表
 * @author lyy
 * @date 2021-7-12 16:02:59
 */
@Data
public class NodePageInput extends AbstractPageRequest {

    @ApiModelProperty(value = "流程id")
    private Long deployId;

}
