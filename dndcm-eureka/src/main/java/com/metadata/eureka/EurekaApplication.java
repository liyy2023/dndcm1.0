package com.metadata.eureka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author lyy
 * @description
 * @date 2021-7-7 10:52:26
 **/
@Slf4j
/*排除数据库自动配置*/
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaServer
public class EurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class,args);
        log.info("eureka注册中心启动成功。。。");
        log.info("注册中心地址:http://localhost:9090");
    }
}
