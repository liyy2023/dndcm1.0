package com.dndcm.project.utils;

import com.dndcm.project.api.entity.ProjectCategory;
import com.dndcm.project.api.entity.vo.ProjectCategoryTree;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lyy
 */
public class TreeUtils {
    /**
     * 结构转换
     * @param t
     * @param tList
     * @return
     */
    public static ProjectCategoryTree covertMenuNode(ProjectCategoryTree t, List<ProjectCategoryTree> tList) {
        List<ProjectCategoryTree> children = tList.stream()
                .filter(subCatalogue -> subCatalogue.getParentId().equals(t.getId()))
                .map(subCatalogue -> covertMenuNode(subCatalogue, tList)).collect(Collectors.toList());
        t.setChildren(children);
        return t;
    }
}
