package com.dndcm.project.service;

import com.dndcm.project.api.entity.Project;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目信息主表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface ProjectService extends IService<Project> {

}
