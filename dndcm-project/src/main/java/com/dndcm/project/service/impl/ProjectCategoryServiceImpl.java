package com.dndcm.project.service.impl;

import com.dndcm.commn.constant.enums.DeleteYnEnum;
import com.dndcm.project.api.entity.ProjectCategory;
import com.dndcm.project.api.entity.vo.ProjectCategoryTree;
import com.dndcm.project.mapper.ProjectCategoryMapper;
import com.dndcm.project.service.ProjectCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dndcm.project.utils.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 项目类别信息表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class ProjectCategoryServiceImpl extends ServiceImpl<ProjectCategoryMapper, ProjectCategory> implements ProjectCategoryService {

    @Autowired
    ProjectCategoryMapper projectCategoryMapper;
    @Override
    public List<ProjectCategoryTree> queryCategoryProcedureTree(ProjectCategory projectCategory) {
        List<ProjectCategoryTree> treeList = projectCategoryMapper.queryCategoryProcedureTree(projectCategory);
        List<ProjectCategoryTree> result = treeList.stream()
                .filter(tCatalogue1 -> tCatalogue1.getParentId().equals(0L))
                .map(menu -> TreeUtils.covertMenuNode(menu, treeList)).collect(Collectors.toList());
        return result;

    }

    @Override
    public ProjectCategoryTree queryCategoryProjectTree(ProjectCategory projectCategory) {
        return null;
    }

    @Override
    public Boolean delete(ProjectCategory projectCategory) {
        projectCategory.setDeleteYn(DeleteYnEnum.YES.getCode());
        return this.updateById(projectCategory);
    }
}
