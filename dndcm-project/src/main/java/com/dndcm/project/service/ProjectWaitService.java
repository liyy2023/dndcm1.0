package com.dndcm.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.project.api.entity.Project;
import com.dndcm.project.api.entity.vo.ProjectWait;

import java.util.List;

public interface ProjectWaitService  extends IService<ProjectWait> {
    List<ProjectWait> projectWait(List<Object> jobIds);
}
