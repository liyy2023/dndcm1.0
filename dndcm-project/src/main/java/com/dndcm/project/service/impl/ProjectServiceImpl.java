package com.dndcm.project.service.impl;

import com.dndcm.project.api.entity.Project;
import com.dndcm.project.mapper.ProjectMapper;
import com.dndcm.project.service.ProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目信息主表  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements ProjectService {

}
