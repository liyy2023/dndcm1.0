package com.dndcm.project.service;

import com.dndcm.project.api.entity.RelationForm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 关联表单  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface RelationFormService extends IService<RelationForm> {

}
