package com.dndcm.project.service.impl;

import com.dndcm.file.api.entity.FileOutput;
import com.dndcm.file.api.feign.FileFeign;
import com.dndcm.project.api.entity.CertificateDirectory;
import com.dndcm.project.mapper.CertificateDirectoryMapper;
import com.dndcm.project.service.CertificateDirectoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 凭证目录  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class CertificateDirectoryServiceImpl extends ServiceImpl<CertificateDirectoryMapper, CertificateDirectory> implements CertificateDirectoryService {
    @Autowired
    CertificateDirectoryMapper certificateDirectoryMapper;

    @Autowired
    FileFeign fileFeign;

    @Override
    public FileOutput uploadCertificate(CertificateDirectory certificateDirectory) {
        FileOutput output=new FileOutput();
         if(certificateDirectory.getFile()!=null&&certificateDirectory.getBucketName()!=null){
             output=fileFeign.uploadFile1(certificateDirectory.getFile(),certificateDirectory.getBucketName());
             certificateDirectory.setFileUrl(output.getViewUrl());
             certificateDirectoryMapper.insert(certificateDirectory);
         }
        return output;
    }
}
