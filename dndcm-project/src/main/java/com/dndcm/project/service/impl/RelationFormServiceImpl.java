package com.dndcm.project.service.impl;

import com.dndcm.project.api.entity.RelationForm;
import com.dndcm.project.mapper.RelationFormMapper;
import com.dndcm.project.service.RelationFormService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 关联表单  服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Service
public class RelationFormServiceImpl extends ServiceImpl<RelationFormMapper, RelationForm> implements RelationFormService {

}
