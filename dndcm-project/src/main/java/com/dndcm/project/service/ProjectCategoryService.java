package com.dndcm.project.service;

import com.dndcm.project.api.entity.ProjectCategory;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dndcm.project.api.entity.vo.ProjectCategoryTree;

import java.util.List;

/**
 * <p>
 * 项目类别信息表  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface ProjectCategoryService extends IService<ProjectCategory> {

    /**
     * 流程树
     * @param projectCategory
     * @return
     */
    List<ProjectCategoryTree> queryCategoryProcedureTree(ProjectCategory projectCategory);

    /**
     * 项目树
     * @param projectCategory
     * @return
     */
    ProjectCategoryTree queryCategoryProjectTree(ProjectCategory projectCategory);

    /**
     * 删除
     * @param projectCategory
     * @return
     */
    Boolean delete(ProjectCategory projectCategory);

}
