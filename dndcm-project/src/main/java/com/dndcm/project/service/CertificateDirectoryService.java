package com.dndcm.project.service;


import com.dndcm.file.api.entity.FileOutput;
import com.dndcm.project.api.entity.CertificateDirectory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 凭证目录  服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
public interface CertificateDirectoryService extends IService<CertificateDirectory> {

     FileOutput uploadCertificate(CertificateDirectory certificateDirectory);
}
