package com.dndcm.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dndcm.project.api.entity.vo.ProjectWait;
import com.dndcm.project.mapper.ProjectWaitMapper;
import com.dndcm.project.service.ProjectWaitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service
public class ProjectWaitServiceImpl extends ServiceImpl<ProjectWaitMapper, ProjectWait> implements ProjectWaitService {
    @Autowired
    ProjectWaitMapper projectWaitMapper;
    @Override
    public List<ProjectWait> projectWait(List<Object> jobIds) {

        return projectWaitMapper.projectWait(jobIds);
    }


}
