package com.dndcm.project.controller;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.project.api.entity.CertificateDirectory;
import com.dndcm.project.service.CertificateDirectoryService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 。。
 * @author ljb
 * 2021-7-12 17:00:00
 */
@Slf4j
@Api(tags = "工具栏任务控制器")
@RestController
@RequestMapping("/toolbarTask")
public class ToolbarTaskController {
    @Autowired
    private CertificateDirectoryService certificateDirectoryService;

    @RequestMapping("/uploadcertificate")
    @ResponseBody
    public CommonResponse upload(@RequestBody CertificateDirectory certificateDirectory) {
        return CommonResponse.ok(certificateDirectoryService.uploadCertificate(certificateDirectory));
    }

}
