package com.dndcm.project.controller;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.project.api.entity.ProjectCategory;
import com.dndcm.project.service.ProjectCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 项目类型目录控制器
 * @author lyy
 * @date 2021-7-12 10:45:25
 */
@Slf4j
@Api(tags = "项目类型制器")
@RestController
@RequestMapping("/projectCategory")
public class ProjectCategoryController {

    @Autowired
    ProjectCategoryService projectCategoryService;

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getProcedureTree")
    @ApiOperation(value = "查询项目流程树",notes = "查询项目流程树")
    public CommonResponse getProcedureTree(@RequestBody ProjectCategory projectCategory){
        return CommonResponse.ok(projectCategoryService.queryCategoryProcedureTree(projectCategory));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/save")
    @ApiOperation(value = "新增",notes = "新增")
    public CommonResponse createProjectCategory(@RequestBody ProjectCategory projectCategory){
        return CommonResponse.ok(projectCategoryService.save(projectCategory));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/edit")
    @ApiOperation(value = "编辑",notes = "编辑")
    public CommonResponse editProjectCategory(@RequestBody ProjectCategory projectCategory){
        return CommonResponse.ok(projectCategoryService.updateById(projectCategory));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/delete")
    @ApiOperation(value = "删除",notes = "删除")
    public CommonResponse deleteProjectCategory(@RequestBody ProjectCategory projectCategory){
        return CommonResponse.ok(projectCategoryService.delete(projectCategory));
    }
}
