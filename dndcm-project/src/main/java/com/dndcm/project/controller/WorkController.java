package com.dndcm.project.controller;

import com.dndcm.project.api.Work;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 。。
 * @author lyy
 * @date 2021-7-7 20:28:53
 */
@Slf4j
@Api(tags = "测试控制器")
@RestController
@RequestMapping("/work")
public class WorkController {

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getWork")
    @ApiOperation(value = "getWork",notes = "getWork")
    Work getWork(@RequestBody Work work) {
        return work;
    }
}
