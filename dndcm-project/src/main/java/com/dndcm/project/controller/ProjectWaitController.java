package com.dndcm.project.controller;

import com.dndcm.commn.response.CommonResponse;
import com.dndcm.project.api.entity.vo.ProjectWait;
import com.dndcm.project.service.ProjectWaitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(tags = "项目1")
@RestController
@RequestMapping("/project")
public class ProjectWaitController {
    @Autowired
    ProjectWaitService projectWaitService;
    @RequestMapping(method = {RequestMethod.POST}, value = "/findToDoWork")
    @ApiOperation(value = "查询待办",notes = "查询待办")
    public CommonResponse projectWait(@RequestParam List<Object> jobIds){
        //待办工作数据
        List<ProjectWait> projectWaits = projectWaitService.projectWait(jobIds);
        Map<String,Object> map = new HashMap<>();
        //总数
        map.put("workTotal",101);
        //即将超期
        map.put("theTimeout",22);
        //已经超期
        map.put("alreadyTimeout",100);
        map.put("findToDoWork",projectWaits);
        return CommonResponse.ok(map);
    }
}
