package com.dndcm.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.project.api.entity.vo.ProjectWait;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
@Mapper
public interface ProjectWaitMapper extends BaseMapper<ProjectWait> {
    List<ProjectWait> projectWait(@Param("jobIds") List<Object> jobIds);
}
