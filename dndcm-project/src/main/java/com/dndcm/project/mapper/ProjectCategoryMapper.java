package com.dndcm.project.mapper;

import com.dndcm.project.api.entity.ProjectCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dndcm.project.api.entity.vo.ProjectCategoryTree;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 项目类别信息表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Mapper
public interface ProjectCategoryMapper extends BaseMapper<ProjectCategory> {

    /**
     * 流程树
     * @param projectCategory
     * @return
     */
    List<ProjectCategoryTree> queryCategoryProcedureTree(ProjectCategory projectCategory);

    /**
     * 项目树
     * @param projectCategory
     * @return
     */
    List<ProjectCategoryTree> queryCategoryProjectTree(ProjectCategory projectCategory);


}
