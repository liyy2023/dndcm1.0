package com.dndcm.project.mapper;

import com.dndcm.project.api.entity.Project;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 项目信息主表  Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-09
 */
@Mapper
public interface ProjectMapper extends BaseMapper<Project> {

}
