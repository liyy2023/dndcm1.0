package com.dndcm.auth.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 登陆用户信息
 * @author lyy
 * @date
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class LoginUser {

    private Long id;
    private String userName;
    private String userPassword;
    private Integer userStatus;
    private List<String> jobs;
}
