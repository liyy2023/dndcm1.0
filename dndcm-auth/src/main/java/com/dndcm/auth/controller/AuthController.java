package com.dndcm.auth.controller;

import com.dndcm.auth.api.vo.SysLoginParam;
import com.dndcm.auth.api.vo.SysLogoutParam;
import com.dndcm.auth.service.SysCaptchaService;
import com.dndcm.auth.service.SysLoginService;
import com.dndcm.commn.response.CommonResponse;
import com.dndcm.sys.api.entity.vo.SysLoginOutput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;


/**
 * 登录鉴权控制器
 * @author lyy
 * @date 2021-7-14 15:58:50
 */
@RestController
@Api(tags = "AuthController", description = "认证中心登录认证")
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    SysCaptchaService sysCaptchaService;

    @Autowired
    SysLoginService sysLoginService;

    @ApiOperation(value = "获取验证码")
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");
        //获取图片验证码
        BufferedImage image = sysCaptchaService.getCaptcha(uuid);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
        IOUtils.closeQuietly(out);
    }

    @ApiOperation(value = "登录以后返回token")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public CommonResponse<SysLoginOutput> login(@RequestBody SysLoginParam sysLoginParam) {
        SysLoginOutput sysLoginOutput = sysLoginService.login(sysLoginParam);
        return CommonResponse.ok(sysLoginOutput);
    }

    @ApiOperation(value = "用户注销登录")
    @PostMapping("/logout")
    public CommonResponse<Boolean> logout(@RequestBody SysLogoutParam sysLogoutParam){
        Boolean flag = sysLoginService.logout(sysLogoutParam);
        return CommonResponse.ok(flag);
    }

    @ApiOperation("校验用户是否存在")
    @PostMapping("/checkUser")
    public CommonResponse<Boolean> checkUser(@RequestBody SysLoginParam sysLoginParam){
        Boolean flag = sysLoginService.checkUser(sysLoginParam);
        return CommonResponse.ok(flag);
    }


}
