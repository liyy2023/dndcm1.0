package com.dndcm.auth.service;

import com.dndcm.auth.api.vo.SysLoginParam;
import com.dndcm.auth.api.vo.SysLogoutParam;
import com.dndcm.sys.api.entity.vo.SysLoginOutput;


/**
 * 系统登录
 * @author lyy
 * @date 2021-7-14 10:20:51
 */
public interface SysLoginService {

    /**
     * @param sysLoginParam
     * @return
     */
    SysLoginOutput login(SysLoginParam sysLoginParam);

    /**
     * @param sysLogoutParam
     * @return
     */
    Boolean logout(SysLogoutParam sysLogoutParam);

    /**
     * @param sysLoginParam
     * @return
     */
    Boolean checkUser(SysLoginParam sysLoginParam);
}
