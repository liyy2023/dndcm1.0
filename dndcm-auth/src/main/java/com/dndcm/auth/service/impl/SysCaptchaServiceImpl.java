package com.dndcm.auth.service.impl;

import com.dndcm.auth.service.SysCaptchaService;
import com.dndcm.commn.exception.DndcmException;
import com.dndcm.commn.exception.ResponseCodeImpl;

import com.google.code.kaptcha.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 系统验证码 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-07-14
 */
@Service
public class SysCaptchaServiceImpl  implements SysCaptchaService {

    @Autowired
    private Producer producer;

    @Autowired
    RedisTemplate<String,String> redisTemplate;

    @Override
    public BufferedImage getCaptcha(String uuid) {
        if(uuid == null){
            throw new DndcmException(new ResponseCodeImpl("9999901111","uuid不能为空"));
        }
        //生成文字验证码
        String code = producer.createText();
        //5分钟后过期
        redisTemplate.boundValueOps(uuid).set(code,300L, TimeUnit.SECONDS);
        return producer.createImage(code);
    }

    @Override
    public boolean validate(String uuid, String code) {
        //从redis中校验
        String cacheCode = redisTemplate.boundValueOps(uuid).get();
        if (cacheCode == null){
            throw new DndcmException(new ResponseCodeImpl("9999901112","验证码已过期"));
        }
        //清除缓存
        redisTemplate.delete(uuid);
        if(cacheCode.equals(code)){
            return true;
        }
        return false;
    }
}
