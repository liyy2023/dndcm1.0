package com.dndcm.auth.service.impl;

import com.dndcm.auth.api.consatant.MessageConstant;
import com.dndcm.auth.domain.LoginUser;
import com.dndcm.auth.domain.SecurityUser;
import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.vo.SysUserVo;
import com.dndcm.sys.api.feign.SysUserFeign;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户管理业务类
 * @author lyy
 * @date 2021-7-14 17:57:42
 */
@Service
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserFeign sysUserFeign;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser queryUser = new SysUser();
        queryUser.setUserName(username);
        SysUserVo userVo = sysUserFeign.loadUserByUserName(queryUser).getData();
        queryUser.setId(userVo.getId());
        List<String> jobs = sysUserFeign.getJobs(queryUser).getData();
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(userVo,loginUser);
        loginUser.setJobs(jobs);
        if (loginUser==null) {
            throw new UsernameNotFoundException(MessageConstant.USERNAME_PASSWORD_ERROR);
        }
        SecurityUser securityUser = new SecurityUser(loginUser);
        if (!securityUser.isEnabled()) {
            throw new DisabledException(MessageConstant.ACCOUNT_DISABLED);
        } else if (!securityUser.isAccountNonLocked()) {
            throw new LockedException(MessageConstant.ACCOUNT_LOCKED);
        } else if (!securityUser.isAccountNonExpired()) {
            throw new AccountExpiredException(MessageConstant.ACCOUNT_EXPIRED);
        } else if (!securityUser.isCredentialsNonExpired()) {
            throw new CredentialsExpiredException(MessageConstant.CREDENTIALS_EXPIRED);
        }
        return securityUser;
    }

}
