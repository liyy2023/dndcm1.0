package com.dndcm.auth.service.impl;

import com.dndcm.auth.api.vo.SysLoginParam;
import com.dndcm.auth.api.vo.SysLogoutParam;
import com.dndcm.auth.domain.SecurityUser;
import com.dndcm.auth.service.SysCaptchaService;
import com.dndcm.auth.service.SysLoginService;
import com.dndcm.auth.service.TokenService;
import com.dndcm.commn.exception.DndcmException;
import com.dndcm.commn.exception.ResponseCodeImpl;
import com.dndcm.sys.api.entity.SysUser;
import com.dndcm.sys.api.entity.vo.SysLoginOutput;

import com.dndcm.sys.api.entity.vo.SysUserVo;
import com.dndcm.sys.api.feign.SysUserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lyy
 */
public class SysLoginServiceImpl implements SysLoginService {

    @Autowired
    SysCaptchaService sysCaptchaService;

    @Autowired
    UserDetailsService userService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    TokenService tokenService;

    @Autowired
    SysUserFeign sysUserFeign;

    @Override
    public SysLoginOutput login(SysLoginParam sysLoginParam) {
        boolean captcha = sysCaptchaService.validate(sysLoginParam.getUuid(), sysLoginParam.getCaptcha());
        if(!captcha){
            throw new DndcmException(new ResponseCodeImpl("9999900222","验证码不正确"));
        }
        //校验用户信息
        SecurityUser user = (SecurityUser) userService.loadUserByUsername(sysLoginParam.getUsername());
        //生成token
        Map<String,String> claim = new HashMap<>();
        claim.put("username", user.getUsername());
        claim.put("userId", user.getId()+"");
        String token = tokenService.createToken(claim);
        //将token存入redis
        redisTemplate.boundHashOps("token").put(user.getId(),token);
        SysLoginOutput output = new SysLoginOutput();
        output.setUserName(user.getUsername());
        output.setToken(token);
        return output;
    }

    @Override
    public Boolean logout(SysLogoutParam sysLogoutParam) {
        redisTemplate.boundHashOps("token").delete(sysLogoutParam.getId());
        return true;
    }

    @Override
    public Boolean checkUser(SysLoginParam sysLoginParam) {
        SysUser checkUser = new SysUser();
        checkUser.setUserName(sysLoginParam.getUsername());
        SysUserVo userVo = sysUserFeign.loadUserByUserName(checkUser).getData();
        if(userVo != null){
            throw new DndcmException(new ResponseCodeImpl("9999900333","用户名已存在"));
        }
        return true;
    }
}
