package com.dndcm.auth.service;

import com.dndcm.auth.utils.TokenUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author lyy
 * @description
 * @date 2021-7-14 15:47:33
 **/
@Component
public class TokenService {
    @Value("${token.private.secret:1q2w3e!@#$abcd}")
    private  String secret;
    @Value("${token.expiration:28800}")
    private Integer expiration;

    public String createToken(Map<String, String> claim){
        return TokenUtil.creatToken(secret,expiration,claim);
    }

    public String getUsername(String token, String key){
        return TokenUtil.getClaimByKey(token,key);
    }

    public Boolean verifierToken(String token,Map<String, String> claim){
        return TokenUtil.verifierToken(secret,token,claim);
    }
}
